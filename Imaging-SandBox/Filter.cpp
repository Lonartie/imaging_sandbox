#include "stdafx.h"
#include "Filter.h"
#include "CommonMacros.h"

QImage Filter::applyFilter(const QImage& image, const std::vector<std::vector<float>>& matrix, float multiplicator)
{
   QImage result(image.size(), image.format());
   OMP_LOOP_IMAGE(image) result.setPixelColor(x, y, applyFilter(image, QPoint(x, y), matrix, multiplicator));
   return result;
}

QColor Filter::applyFilter(const QImage& image, const QPoint& position, const std::vector<std::vector<float>>& matrix, float multiplicator)
{
   float red = 0.0f;
   float green = 0.0f;
   float blue = 0.0f;

   int abx = std::floor(matrix[0].size() / 2.0);
   int aby = std::floor(matrix.size() / 2.0);

   int _x, _y, av;
   QColor pix;

   _x = 0;
   for (int x = position.x() - abx; x <= position.x() + abx; x++)
   {
      _y = 0;
      for (int y = position.y() - aby; y <= position.y() + aby; y++)
      {
         if (x < 0 || y < 0) continue;
         if (x > image.width() - 1 || y > image.height() - 1) continue;

         pix = image.pixelColor(x, y);

         red += pix.red() * matrix[_y][_x];
         green += pix.green() * matrix[_y][_x];
         blue += pix.blue() * matrix[_y][_x];

         _y++;
      }
      _x++;
   }

   red *= multiplicator / 255.0;
   green *= multiplicator / 255.0;
   blue *= multiplicator / 255.0;

   return QColor
   (
      std::max(0.0f, std::min(1.0f, red)) * 255,
      std::max(0.0f, std::min(1.0f, green)) * 255,
      std::max(0.0f, std::min(1.0f, blue)) * 255
   );
}

float Filter::applyFilter(const QImage& image, const QPoint& position, const std::vector<std::vector<float>>& matrix, float multiplicator, ColorChannel channel)
{
   float weight = 0.0f;
   int abx = std::floor(matrix[0].size() / 2.0);
   int aby = std::floor(matrix.size() / 2.0);

   int _x, _y, av;
   QColor pix;

   _x = 0;
   for (int x = position.x() - abx; x <= position.x() + abx; x++)
   {
      _y = 0;
      for (int y = position.y() - aby; y <= position.y() + aby; y++)
      {
         if (x < 0 || y < 0) continue;
         if (x > image.width() - 1 || y > image.height() - 1) continue;

         pix = image.pixelColor(x, y);

         av = channel == Red ? pix.red()
            : channel == Green ? pix.green()
            : channel == Blue ? pix.blue()
            : channel == Alpha ? pix.alpha()
            : (pix.red() + pix.green() + pix.blue()) / 3;

         weight += av * matrix[_y][_x];

         _y++;
      }
      _x++;
   }

   weight *= multiplicator / 255.0;

   return std::max(0.0f, std::min(1.0f, weight));
}
