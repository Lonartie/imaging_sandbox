#pragma once

#include "CommonFunctions.h"

class Negative: public Operation
{
   Q_OBJECT;
   OPERATION(Negative);

public:

   virtual ImageData processImage(const ImageData& image) const override;

   virtual ImageType getImageType() const override;

};
