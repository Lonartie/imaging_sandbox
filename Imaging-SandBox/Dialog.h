#pragma once

#include "stdafx.h"

class Dialog : public QDialog
{
   Q_OBJECT

public:
    Dialog(QWidget* parent, QWidget* content);
    ~Dialog() = default;
};
