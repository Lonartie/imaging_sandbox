#pragma once

#include "CommonFunctions.h"

class Mirror : public Operation
{
   Q_OBJECT;
   OPERATION(Mirror);

public:
     
   Mirror();

   virtual ImageData processImage(const ImageData& image) const override;

   virtual ImageType getImageType() const override;

private:

   QString m_axis;

};
