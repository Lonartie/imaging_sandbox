#include "stdafx.h"
#include "Negative.h"

ImageData Negative::processImage(const ImageData& image) const
{
   if (!image.isQImage()) return QImage();
   auto& img = image.getQImage();
   QImage result(img.size(), img.format());

   OMP_LOOP_IMAGE(result)
   {
      auto pix = img.pixelColor(x, y);

      result.setPixelColor(x, y, QColor
      (
         255 - pix.red(),
         255 - pix.green(),
         255 - pix.blue()
      ));
   }

   return result;
}

ImageType Negative::getImageType() const
{
   return ImageType::QImage;
}

REGISTER_OPERATION(Negative);
