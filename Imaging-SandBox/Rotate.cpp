#include "stdafx.h"
#include "Rotate.h"

#define PI 3.14159

Rotate::Rotate()
{
   REGISTER_PARAMETER(new IntegerSlider("degree: ", -180, 180, 1, &m_degree, QStringLiteral("�")));
   REGISTER_PARAMETER(new InterpolationSelection(&m_interpolation));
}

ImageData Rotate::processImage(const ImageData& image) const
{
   if (!image.isQImage()) return QImage();
   auto& img = image.getQImage();

   QPointF mid_in(img.width() / 2.0, img.height() / 2.0);
   
   auto tl = rotate({img.width() / -2.0, img.height() / -2.0});
   auto tr = rotate({img.width() / 2.0, img.height() / -2.0});
   auto bl = rotate({img.width() / -2.0, img.height() / 2.0});
   auto br = rotate({img.width() / 2.0, img.height() / 2.0});

   auto min_x = std::min(tl.x(), std::min(tr.x(), std::min(bl.x(), br.x())));
   auto max_x = std::max(tl.x(), std::max(tr.x(), std::max(bl.x(), br.x())));
   auto min_y = std::min(tl.y(), std::min(tr.y(), std::min(bl.y(), br.y())));
   auto max_y = std::max(tl.y(), std::max(tr.y(), std::max(bl.y(), br.y())));

   auto width = max_x - min_x;
   auto height = max_y - min_y;

   QImage result(width, height, QImage::Format_ARGB32);
   result.fill(Qt::transparent);
   QPointF mid_out(width / 2.0, height / 2.0);

   OMP_LOOP_IMAGE(result)
   {
      auto tp = rotate(QPointF(x, y) - mid_out) + mid_in;
      QColor col;
      if (tp.x() < 0 || tp.x() >= img.width() || tp.y() < 0 || tp.y() >= img.height())
         col = Qt::transparent;
      else
         col = m_interpolation->interpolate(img, tp);
      if (!col.isValid()) col = Qt::transparent;
      result.setPixelColor(x, y, col);
   }

   return result;
}

ImageType Rotate::getImageType() const
{
   return ImageType::QImage;
}

QPointF Rotate::rotate(const QPointF& p) const
{
   double rad = m_degree * PI / 180.0;
   return
   {
      p.x() * std::cos(rad) - p.y() * std::sin(rad),
      p.x() * std::sin(rad) + p.y() * std::cos(rad)
   };
}

REGISTER_OPERATION(Rotate);
