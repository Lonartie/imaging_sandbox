#include "stdafx.h"
#include "FilterCl.h"
#include "CLMath.h"

using namespace EasyOCL;

KERNEL_CLASS_CPP_BEGIN(FilterCl);

#ifndef NDEBUG
KERNEL_REGISTER_MACRO_CONSTANT(FilterCl, applyFilter, IS_DEBUG, true, bool);
#else
KERNEL_REGISTER_MACRO_CONSTANT(FilterCl, applyFilter, IS_DEBUG, false, bool);
#endif

KERNEL_FUNCTION_CPP(FilterCl, applyFilter, (const unsigned char*, image_in, unsigned char*, image_out, const int*, imageSize, const float*, kernelMatrix, const int*, kernelSize),
{  
int A = 3;
int R = 0;
int G = 1;
int B = 2;

int id = get_global_id(0);
	
int image_width = imageSize[0];
int image_height = imageSize[1];

int kernel_width = kernelSize[0];
int kernel_height = kernel_width;
int kernel_mid = (kernel_width + 1) / 2;

int x = id % image_width;
int y = id / image_width;

float w_r = 0.0f;
float w_g = 0.0f;
float w_b = 0.0f;

for (int _x = 0; _x < kernel_width; _x++)
{
	for (int _y = 0; _y < kernel_height; _y++)
	{
		int image_x = x - kernel_mid + _x;
		int image_y = y - kernel_mid + _y;

		if (image_x < 0 || image_y < 0) continue;
		if (image_x >= image_width || image_y >= image_height) continue;

		int pixel_index = CLMath::xy_to_index(image_x, image_y, image_width);

		float r = image_in[pixel_index * 4 + R] / 255.0f;
		float g = image_in[pixel_index * 4 + G] / 255.0f;
		float b = image_in[pixel_index * 4 + B] / 255.0f;

		float kernel_weight = kernelMatrix[CLMath::xy_to_index(_x, _y, kernel_width)];

		w_r += r * kernel_weight;
		w_g += g * kernel_weight;
		w_b += b * kernel_weight;
	}
}

w_r = CLMath::clamp_between(0.0f, 1.0f, w_r);
w_g = CLMath::clamp_between(0.0f, 1.0f, w_g);
w_b = CLMath::clamp_between(0.0f, 1.0f, w_b);

image_out[id * 4 + A] = 0;
image_out[id * 4 + R] = w_r * 255;
image_out[id * 4 + G] = w_g * 255;
image_out[id * 4 + B] = w_b * 255;

}, CLMath);

KERNEL_CLASS_CPP_END();