#pragma once

#include "CommonFunctions.h"

class BilinearInterpolation : public Interpolation
{
	Q_OBJECT;
	INTERPOLATION(BilinearInterpolation);

public:

	virtual QColor interpolate(const QImage& image, const QPointF& required) const override;

private:

	QPoint clamp(const QSize& size, const QPoint& point) const;
	QColor faded(const QColor& a, const QColor& b, float value) const;
};
