#include "stdafx.h"
#include "Dialog.h"

Dialog::Dialog(QWidget* parent, QWidget* content)
   : QDialog(parent)
{
   setWindowTitle(content->windowTitle());

   auto layout = new QGridLayout();
   layout->addWidget(content);
   setLayout(layout);

   connect(content, &QWidget::hide, this, &QDialog::close);

   adjustSize();
}