#include "stdafx.h"
#include "EdgeDetection.h"

EdgeDetection::EdgeDetection()
{
   REGISTER_PARAMETER(new ColorSelection("tone: ", &m_color));
   REGISTER_PARAMETER(new IntegerSlider("intensity: ", 0, 1000, 1, &m_intense, "%"));

   kernel = {
      {-1, -1, -1},
      {-1,  8, -1},
      {-1, -1, -1}
   };
}

ImageData EdgeDetection::processImage(const ImageData& imageData) const
{
	if (!imageData.isQImage()) return QImage();
	const QImage& image = imageData.getQImage();

   QImage result(image.size(), image.format());

   OMP_LOOP_IMAGE(image)
   {
      float weight = getWeight(image, QPoint(x, y));

      int red = m_color.red() * weight;
      int green = m_color.green() * weight;
      int blue = m_color.blue() * weight;

      result.setPixelColor(x, y, QColor(red, green, blue));
   }

   return result;
}

ImageType EdgeDetection::getImageType() const
{
   return ImageType::QImage;
}

float EdgeDetection::getWeight(const QImage& image, const QPoint& position) const
{
   return Filter::applyFilter(image, position, kernel, m_intense / 100.0, Filter::GrayScale);
}

int EdgeDetection::clamp(int pix) const
{
   return std::max(0, std::min(1, pix));
}

float EdgeDetection::fade(float a, float b, float p) const
{
   return a + (b - a) * p;
}

REGISTER_OPERATION(EdgeDetection);
