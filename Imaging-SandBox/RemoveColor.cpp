#include "stdafx.h"
#include "RemoveColor.h"

const QString RemoveColor::remove_selected = "selected color";
const QString RemoveColor::remove_others = "every other color";
const QString RemoveColor::hard_removal = "hard removal";
const QString RemoveColor::soft_removal = "faded removal";

RemoveColor::RemoveColor()
{
	REGISTER_PARAMETER(new ComboBox("color removal: ", {remove_selected, remove_others}, &m_removal));
	REGISTER_PARAMETER(new ComboBox("removal type: ", {hard_removal, soft_removal}, &m_type));
	REGISTER_PARAMETER(new ColorSelection("selected color: ", &m_color));
	REGISTER_PARAMETER(new IntegerSlider("color tolerance: ", 0, 100, 1, &m_tolerance, "%"));
}

ImageData RemoveColor::processImage(const ImageData& imageData) const
{
	if (!imageData.isQImage()) return QImage();
	const QImage& image = imageData.getQImage();
	
	QImage result(image.size(), image.format());

	OMP_LOOP_IMAGE(image)
	{
		auto pix = image.pixelColor(x, y);

		if (m_removal == remove_selected)
			pix = removeSelected(pix);
		else if (m_removal == remove_others)
			pix = removeOthers(pix);

		result.setPixelColor(x, y, pix);
	}

	return result;
}

ImageType RemoveColor::getImageType() const
{
	return ImageType::QImage;
}

QColor RemoveColor::removeSelected(const QColor& col) const
{
	auto av = (col.red() + col.green() + col.blue()) / 3.0;
	if (isInTolerance(col) && m_type == hard_removal)
		return QColor(av, av, av);
	return fade(col, av, m_tolerance / 100.0 * distance(m_color, col));
}

QColor RemoveColor::removeOthers(const QColor& col) const
{
	auto av = (col.red() + col.green() + col.blue()) / 3.0;
	if (!isInTolerance(col) && m_type == hard_removal)
		return QColor(av, av, av);
	return fade(av, col, m_tolerance / 100.0 * distance(m_color, col));
}

bool RemoveColor::isInTolerance(const QColor& col) const
{
	return
		m_color.red() - (m_color.red() * m_tolerance / 100.0) < col.red() && col.red() < m_color.red() + ((255 - m_color.red()) * m_tolerance / 100.0) &&
		m_color.green() - (m_color.green() * m_tolerance / 100.0) < col.green() && col.green() < m_color.green() + ((255 - m_color.green()) * m_tolerance / 100.0) &&
		m_color.blue() - (m_color.blue() * m_tolerance / 100.0) < col.blue() && col.blue() < m_color.blue() + ((255 - m_color.blue()) * m_tolerance / 100.0);
}

double RemoveColor::distance(const QColor& a, const QColor& b) const
{
	return
		(std::abs(a.red() - b.red()) +
		 std::abs(a.green() - b.green()) +
		 std::abs(a.blue() - b.blue())) / (3 * 255.0);
}

QColor RemoveColor::fade(const QColor& a, const QColor& b, float value) const
{
	return QColor
	(
		a.red() + ((b.red() - a.red()) * value),
		a.green() + ((b.green() - a.green()) * value),
		a.blue() + ((b.blue() - a.blue()) * value),
		a.alpha() + ((b.alpha() - a.alpha()) * value)
	);
}

REGISTER_OPERATION(RemoveColor);