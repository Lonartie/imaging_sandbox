#include "stdafx.h"
#include "AdvancedLabel.h"

AdvancedLabel::AdvancedLabel(QWidget* parent)
	: QLabel(parent)
{
	setContextMenuPolicy(Qt::CustomContextMenu);
}

AdvancedLabel::AdvancedLabel(const QImage& image)
	: QLabel(nullptr)
{
	setContextMenuPolicy(Qt::CustomContextMenu);
	setPixmap(QPixmap::fromImage(image.scaled(QSize(150, 150), Qt::KeepAspectRatio)));
	m_image = image;
}

void AdvancedLabel::setFullResolution(bool full)
{
	m_fullResolution = full;
	if (full)
		setPixmap(QPixmap::fromImage(m_image));
	else
		setPixmap(QPixmap::fromImage(m_image.scaled(QSize(150, 150), Qt::KeepAspectRatio)));
}

void AdvancedLabel::setDraggable(bool draggable)
{
	m_draggable = draggable;
}

void AdvancedLabel::setImage(const QImage& image)
{
	m_image = image;

	if (m_grayed)
		setImageInUI(grayed(scaledImage()));
	else
		setImageInUI(scaledImage());
}

void AdvancedLabel::setDataSuffix(const QString& suffix)
{
	m_dataSuffix = suffix;
}

void AdvancedLabel::setContextMenu(QMenu* menu)
{
	m_contextMenu = menu;
}

void AdvancedLabel::setData(const QVariant& variant)
{
	m_data = variant;
}

const QVariant& AdvancedLabel::getData() const
{
	return m_data;
}

QVariant& AdvancedLabel::getData()
{
	return m_data;
}

void AdvancedLabel::grayOut(bool gray)
{
	m_grayed = gray;
	if (gray)
		setImageInUI(grayed(scaledImage()));
	else
		setImageInUI(scaledImage());
}

void AdvancedLabel::mousePressEvent(QMouseEvent* event)
{
	emit clicked(event->pos(), event->button());

	if (event->button() == Qt::LeftButton)
	{
		if (!m_draggable) return;

		const QPixmap* _pixmap = this->pixmap();
		QPixmap pixmap;

		if (_pixmap && !_pixmap->isNull())
			pixmap = *_pixmap;
		else if (parent())
		{
			auto _parent = qobject_cast<QWidget*>(parent());
			if (_parent)
			{
				pixmap = QPixmap(_parent->size());
				_parent->render(&pixmap, QPoint(), QRegion(_parent->rect()));
			}
		}

		QByteArray itemData;
		QDataStream dataStream(&itemData, QIODevice::WriteOnly);

		if (_pixmap && !_pixmap->isNull())
		{
			auto ptr = &m_image;
			const char* addr = static_cast<const char*>(static_cast<void*>(&ptr));
			QByteArray image_ptr(addr, sizeof(QImage*));
			dataStream << image_ptr << QPoint(event->pos());
		}

		QMimeData* m_qMimeData = new QMimeData;
		m_qMimeData->setData("application/x-dnditemdata" + m_dataSuffix, itemData);

		QDrag* m_qDrag = new QDrag(this);
		m_qDrag->setMimeData(m_qMimeData);
		m_qDrag->setPixmap(pixmap);

		auto pos = mapFromGlobal(QCursor::pos());

		m_qDrag->setHotSpot(event->pos());

		if (m_qDrag->exec(Qt::CopyAction, Qt::CopyAction) == Qt::CopyAction)
			emit dragAccepted();
	}
	else if (event->button() == Qt::RightButton)
	{
		showContextMenu();
	}
}

void AdvancedLabel::mouseDoubleClickEvent(QMouseEvent *event)
{
	emit doubleClicked(event->pos());
}

void AdvancedLabel::enterEvent(QEvent* event)
{
	emit mouseEntered(QCursor::pos());
}

void AdvancedLabel::leaveEvent(QEvent* event)
{
	emit mouseLeaved();
}

void AdvancedLabel::showContextMenu()
{
	if (!m_contextMenu) return;

	m_contextMenu->exec(QCursor::pos());
}

void AdvancedLabel::setImageInUI(const QImage& image)
{
	setPixmap(QPixmap::fromImage(image));
}

QImage AdvancedLabel::grayed(const QImage& image)
{
	if (image.width() <= 0 || image.height() <= 0 || image.isNull())
		return QImage();

	QImage result(image.width(), image.height(), image.format());

	OMP_LOOP_IMAGE(image)
	{
		QColor res = image.pixelColor(x, y);

		res.setRed(res.red() * .3);
		res.setGreen(res.green() * .3);
		res.setBlue(res.blue() * .3);

		result.setPixelColor(x, y, res);
	}

	return result;
}

QImage AdvancedLabel::scaledImage() const
{
	if (m_fullResolution)
		return m_image;
	else
		return m_image.scaled(QSize(150, 150), Qt::KeepAspectRatio);
}
