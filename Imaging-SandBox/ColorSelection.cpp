#include "stdafx.h"
#include "ColorSelection.h"

namespace
{
   static const QString sheet = ".AdvancedFrame { background-color: #%1; border: 1px solid black; border-radius: 10px}";
}

ColorSelection::ColorSelection(const QString& text, QColor* connected)
   : m_text(text)
   , m_connected(connected)
{
   if (connected) *connected = Qt::white;
}

QWidget* ColorSelection::widget(QWidget* parent /*= Q_NULLPTR*/)
{
   auto container = new QWidget(parent);
   auto layout = new QHBoxLayout();
   auto desc = new QLabel();
   auto fram = new AdvancedFrame();

   layout->setMargin(0);
   layout->setSpacing(0);

   desc->setText(m_text);
   fram->setFixedSize(40, 40);

   connect(fram, &AdvancedFrame::clicked, std::bind(&ColorSelection::selectColor, this, fram));

   fram->setStyleSheet(sheet.arg(m_connected->rgba(), 8, 16));
   
   layout->addWidget(desc);
   layout->addWidget(fram);

   container->setLayout(layout);
   return container;
}

void ColorSelection::selectColor(AdvancedFrame* fram)
{
   QColorDialog diag;
   
   QColor tmp = Qt::white;
   if (m_connected) tmp = *m_connected;

   diag.setCurrentColor(tmp);
   connect(&diag, &QColorDialog::currentColorChanged, this, [=](auto& col)
   {
      *m_connected = col;
      emit updated();
      fram->setStyleSheet(sheet.arg(m_connected->rgba(), 8, 16));
   });
      
   diag.setOptions(QColorDialog::ShowAlphaChannel);

   if (diag.exec() == QDialog::Accepted)
   {
      *m_connected = diag.selectedColor();
   }
   else
   {
      *m_connected = tmp;
   }

   emit updated();
   fram->setStyleSheet(sheet.arg(m_connected->rgba(), 8, 16));
}