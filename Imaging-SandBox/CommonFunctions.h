#pragma once

#include "stdafx.h"
#include "CommonMacros.h"
#include "OperationParameter.h"

#include "Filter.h"

#include "Interpolation.h"
#include "InterpolationRegistry.h"

#include "Operation.h"
#include "OperationRegistry.h"

#include "IntegerSlider.h"
#include "InterpolationSelection.h"
#include "ImageSelection.h"
#include "ColorSelection.h"
#include "ComboBox.h"
#include "Checkbox.h"