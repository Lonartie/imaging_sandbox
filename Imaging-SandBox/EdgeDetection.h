#pragma once

#include "CommonFunctions.h"

class EdgeDetection: public Operation
{
   Q_OBJECT;
   OPERATION(EdgeDetection);

public:

   EdgeDetection();

	virtual ImageData processImage(const ImageData& imageData) const override;

   virtual ImageType getImageType() const override;

private /*functions*/:

   float getWeight(const QImage& image, const QPoint& position) const;
   int clamp(int pix) const;
   float fade(float a, float b, float p) const;

private /*members*/:

   QColor m_color;
   int m_intense = 100;
   std::vector<std::vector<float>> kernel;
};
