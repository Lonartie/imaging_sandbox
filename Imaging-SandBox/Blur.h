#pragma once

#include "CommonFunctions.h"

class Blur: public Operation
{
   Q_OBJECT;
   OPERATION(Blur);

public:

   Blur();

   virtual ImageData processImage(const ImageData& imageData) const override;

   virtual ImageType getImageType() const override;

private:

   std::vector<float> createBlurMatrix() const;
   float gaussian(float x, float y, float sig) const;

   int m_intensity = 5;
   int m_iterations = 1;
	bool m_ocl = false;
};
