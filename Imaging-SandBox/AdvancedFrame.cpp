#include "stdafx.h"
#include "AdvancedFrame.h"

AdvancedFrame::AdvancedFrame(QWidget* parent /*= Q_NULLPTR*/)
	: QFrame(parent)
{}

void AdvancedFrame::setDraggable(bool draggable)
{
	m_draggable = draggable;
}

void AdvancedFrame::setContextMenu(QMenu* menu)
{
	m_contextMenu = menu;
}

void AdvancedFrame::setDataSuffix(const QString& suffix)
{
	m_dataSuffix = suffix;
}

void AdvancedFrame::setAdditionalData(const QVariant& data)
{
	m_additionalData = data;
}

void AdvancedFrame::mousePressEvent(QMouseEvent * event)
{
   emit clicked(event->pos());

	if (event->button() == Qt::LeftButton)
	{
		if (!m_draggable) return;

		QPixmap pixmap(size());
		render(&pixmap, QPoint(), QRegion(rect()));

		QByteArray itemData;
		QDataStream dataStream(&itemData, QIODevice::WriteOnly);
		dataStream << pixmap << m_additionalData << QPoint(event->pos());

		QMimeData* m_qMimeData = new QMimeData;
		m_qMimeData->setData("application/x-dnditemdata" + m_dataSuffix, itemData);

		QDrag* m_qDrag = new QDrag(this);
		m_qDrag->setMimeData(m_qMimeData);
		m_qDrag->setPixmap(pixmap);
		m_qDrag->setHotSpot(event->pos());

		QPixmap tempPixmap = pixmap;
		QPainter painter;
		painter.begin(&tempPixmap);
		painter.fillRect(pixmap.rect(), QColor(127, 127, 127, 127));
		painter.end();

		if (m_qDrag->exec(Qt::CopyAction, Qt::CopyAction) == Qt::CopyAction)
			emit dragAccepted();
	}
	else if (event->button() == Qt::RightButton)
	{
		if (m_contextMenu)
		{
			m_contextMenu->exec(QCursor::pos());
		}
	}
}

void AdvancedFrame::enterEvent(QEvent* /*event*/)
{
	emit mouseEntered();
}

void AdvancedFrame::leaveEvent(QEvent* /*event*/)
{
	emit mouseLeaved();
}

void AdvancedFrame::mouseDoubleClickEvent(QMouseEvent* event)
{
	emit doubleClicked(event->pos());
}
