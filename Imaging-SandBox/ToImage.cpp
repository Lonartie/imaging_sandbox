#include "stdafx.h"
#include "ToImage.h"
#include "FrequencyImage.h"

ImageData ToImage::processImage(const ImageData& imageData) const
{
	if (!imageData.isFreqImage()) return ImageData();
	const FrequencyImage& image = imageData.getFreqImage();

   int width = image.width();
   int height = image.height();
   std::size_t size = width * height;

	const FrequencyImage& in = image;
	FrequencyImage out(QSize(image.width(), image.height()));

   auto pr = fftw_plan_dft_2d(width, height, in.getChannel(Filter::Red), out.getChannel(Filter::Red), FFTW_BACKWARD, FFTW_ESTIMATE);
   auto pg = fftw_plan_dft_2d(width, height, in.getChannel(Filter::Green), out.getChannel(Filter::Green), FFTW_BACKWARD, FFTW_ESTIMATE);
   auto pb = fftw_plan_dft_2d(width, height, in.getChannel(Filter::Blue), out.getChannel(Filter::Blue), FFTW_BACKWARD, FFTW_ESTIMATE);

   fftw_execute(pr);
   fftw_execute(pg);
   fftw_execute(pb);

   fftw_destroy_plan(pr);
   fftw_destroy_plan(pg);
   fftw_destroy_plan(pb);

   return out.toImage();
}

void ToImage::imageToComplexChannels(const QImage& image, fftw_complex* out, Filter::ColorChannel channel) const
{
   int width = image.width();
   int height = image.height();
   std::size_t size = width * height;

   OMP_LOOP_IMAGE(image)
   {
      std::size_t index = y * width + x;
      auto pix = image.pixelColor(x, y);

      if (channel == Filter::Red)      out[index][1] = pix.red() / 255.0;
      if (channel == Filter::Green)    out[index][1] = pix.green() / 255.0;
      if (channel == Filter::Blue)     out[index][1] = pix.blue() / 255.0;

      out[index][0] = 0.0;
   }
}

QImage ToImage::imageFromComplexChannels(const std::vector<fftw_complex*>& channels, int width, int height) const
{
   QImage result(width, height, QImage::Format_ARGB32);

   OMP_LOOP_IMAGE(result)
   {
      std::size_t index = y * width + x;
      int red = std::max<int>(0, std::min<int>(255, channels[0][index][1]));
      int green = std::max<int>(0, std::min<int>(255, channels[1][index][1]));
      int blue = std::max<int>(0, std::min<int>(255, channels[2][index][1]));
      result.setPixelColor(x, y, QColor(red, green, blue));
   }

   return result;
}

ImageType ToImage::getImageType() const
{
   return ImageType::FImage;
}

REGISTER_OPERATION(ToImage);