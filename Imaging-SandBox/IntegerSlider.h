#pragma once

#include "stdafx.h"
#include "OperationParameter.h"

class IntegerSlider : public OperationParameter
{
	Q_OBJECT;

public:
	IntegerSlider(const QString& fieldName, int min = 0, int max = 100, int step = 1, int* connected = nullptr, const QString& unit = "");
	virtual ~IntegerSlider() = default;

	void connectValue(int* value);

	virtual QWidget* widget(QWidget* parent = Q_NULLPTR) override;

private:
	QString name;
	QString unit;
	int min, max, step;
	int* value = nullptr;
};
