﻿#include "stdafx.h"
#include "ProcessingPipeline.h"
#include "ImageView.h"

extern bool TouchDevice;


int ProcessingPipeline::instance = 0;

ProcessingPipeline::ProcessingPipeline(QWidget* parent)
	: QWidget(parent)
{
	m_ui.setupUi(this);

	m_activeLayout = m_ui.PipelineContent;

	setAcceptDrops(true);
	if (TouchDevice) QScroller::grabGesture(m_ui.scrollArea, QScroller::LeftMouseButtonGesture);

	connect(m_ui.addButton, &QPushButton::pressed, this, &ProcessingPipeline::addNewInstance, Qt::QueuedConnection);
	connect(m_ui.removeButton, &QPushButton::pressed, this, &ProcessingPipeline::removeInstance, Qt::QueuedConnection);

	if (instance == 0)
	{
		m_ui.removeButton->setEnabled(false);
	}

	instance++;
}

ProcessingPipeline::~ProcessingPipeline()
{
	instance--;
}

void ProcessingPipeline::setImage(const QImage& image)
{
	m_image = image;
	setInputImageInUI();
}

void ProcessingPipeline::addOperation(Operation* operation)
{
	if (operation == nullptr)
	{
		return;
	}

	operation = operation->copy();
	addOperationInUI(operation);
	reconnectAllOperations();
	calculatePreview();
}

void ProcessingPipeline::insertOperation(Operation* operation, int index)
{
	if (operation == nullptr)
	{
		return;
	}

	operation = operation->copy();
	insertOperationInUI(operation, index);
	reconnectAllOperations();
	calculatePreview();
}

void ProcessingPipeline::removeOperation(Operation* operation)
{
	int index = 0;
	for (auto& current : m_operations)
	{
		if (current.first == operation)
		{
			for (auto& item : current.second.second)
			{
				removeUIElement(item);
			}
			break;
		}
		index++;
	}

	m_operations.erase(m_operations.begin() + index);
	reconnectAllOperations();
	calculatePreview();
}

void ProcessingPipeline::process(const QString& dir, int index)
{
	if (!m_operations.size())
	{
		return;
	}

	m_outputPath = dir;
	m_isRealProcessing = true;
	m_currentIndex = index;

	m_operations.first().first->execute(m_image);

	connect(this, &ProcessingPipeline::processingDone, this, &ProcessingPipeline::saveProcessingOutcome);
}

void ProcessingPipeline::imageProcessed(int operationIndex, const ImageData& image)
{
	if (m_operations.size() <= operationIndex)
	{
		m_isRealProcessing = false;
		return;
	}

	auto operation = m_operations[operationIndex].first;

	if (!m_isRealProcessing)
	{
		showImageForOperation(operation, image);
	}

	if (m_operations.size() <= operationIndex + 1 || (m_operations[operationIndex].first == m_cancelOperation && m_cancelOperation != nullptr))
	{
		if (m_isRealProcessing)
		{
			emit processingDone(image);
			m_isRealProcessing = false;
		}
		return;
	}

	auto nextOperation = m_operations[operationIndex + 1].first;

	if (m_isRealProcessing || image.width() < 150)
	{
		nextOperation->execute(image);
	}
	else
	{
		nextOperation->execute(image/*.scaled(QSize(150, 150), Qt::KeepAspectRatio)*/);
	}
}

void ProcessingPipeline::dragEnterEvent(QDragEnterEvent* event)
{
	auto formats = event->mimeData()->formats();

	if (event->mimeData()->hasFormat("application/x-dnditemdata")
		 || event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist"))
		event->acceptProposedAction();
}

void ProcessingPipeline::dropEvent(QDropEvent* event)
{
	const QMimeData* mime = event->mimeData();

	if (mime->hasFormat("application/x-dnditemdata"))
	{
		auto data = mime->data("application/x-dnditemdata");
		QDataStream stream(&data, QIODevice::ReadOnly);
		QByteArray image_ptr;
		stream >> image_ptr;

		setImage(*(*static_cast<QImage**>(static_cast<void*>(image_ptr.data()))));
		event->acceptProposedAction();
	}
	else if (mime->hasFormat("application/x-qabstractitemmodeldatalist"))
	{
		auto data = mime->data("application/x-qabstractitemmodeldatalist");
		QDataStream stream(&data, QIODevice::ReadOnly);
		int row, col;
		QMap<int, QVariant> roleDataMap;
		stream >> row >> col >> roleDataMap;

		auto operation_addr = roleDataMap[Qt::UserRole + 1].toByteArray();
		auto operation = (*static_cast<Operation**>(static_cast<void*>(operation_addr.data())));

		auto pos = QCursor::pos();
		auto index = 0;
		auto releasedOnWidget = operationAtPos(pos, &index) != nullptr;

		if (operation)
		{
			if (!releasedOnWidget)
			{
				addOperation(operation);
			}
			else
			{
				insertOperation(operation, index);
			}
		}

		event->acceptProposedAction();
	}
}

void ProcessingPipeline::mousePressEvent(QMouseEvent* event)
{
	if (event->button() == Qt::RightButton)
	{
		auto menu = new QMenu(this);

		auto item1 = new QAction("remove all operations");
		connect(item1, &QAction::triggered, this, &ProcessingPipeline::removeAllOperations);

		auto all = new QMenu("all pipelines");
		auto aitem1 = new QAction("remove all operations");
		connect(aitem1, &QAction::triggered, this, &ProcessingPipeline::removeAllOperationsFromAllPipelines);

		all->addAction(aitem1);

		menu->addAction(item1);
		menu->addSeparator();
		menu->addMenu(all);

		menu->exec(QCursor::pos());
	}
}

void ProcessingPipeline::removeAllOperations()
{
	for (auto& operation : m_operations)
	{
		for (auto& widget : operation.second.second)
		{
			removeUIElement(widget);
		}

		delete operation.first;
	}

	m_operations.clear();
}

void ProcessingPipeline::setOrientation(Qt::Orientation orientation)
{
	m_ui.gridLayout_2->removeItem(m_activeLayout);

	QBoxLayout* newLayout;

	if (orientation == Qt::Orientation::Horizontal)
		newLayout = new QHBoxLayout();
	else
		newLayout = new QVBoxLayout();

	newLayout->setAlignment(Qt::AlignCenter);
	newLayout->setSpacing(0);
	newLayout->setMargin(0);
	newLayout->setParent(m_ui.gridLayout_2);

	for (int i = 0; i < m_activeLayout->count(); i++)
	{
		auto widget = m_activeLayout->itemAt(i)->widget();
		newLayout->addItem(m_activeLayout->itemAt(i));

		if (widget->objectName() == "arrow")
		{
			auto frm = qobject_cast<AdvancedFrame*>(widget);
			if (!frm) continue;
			auto lbl = qobject_cast<AdvancedLabel*>(frm->layout()->itemAt(0)->widget());
			if (!lbl) continue;

			if (orientation == Qt::Vertical)
				lbl->setText(QString(QChar(0x25BC)));
			else
				lbl->setText(QString(QChar(0x25BA)));

		}
	}

	m_activeLayout = newLayout;
	m_orientation = orientation;
	m_ui.gridLayout_2->addItem(newLayout, 1, 1, 1, 1);
	m_activeLayout->update();
}

void ProcessingPipeline::imageSelection(bool on)
{
	m_imageSelectionActive = on;

	if (m_inputView)
	{
		m_inputView->grayOut(on);
	}

	for (auto& operation : m_operations)
	{
		operation.second.first->grayOut(on);

		auto frame = operation.second.second[1];
		if (on)
			frame->setStyleSheet(".AdvancedFrame { background-color: lightgray; border: 1px solid black; border-radius: 10px}");
		else
			frame->setStyleSheet(".AdvancedFrame { background-color: white; border: 1px solid black; border-radius: 10px}");
	}
}

void ProcessingPipeline::removeOperationMode(Mode mode, Operation* operation)
{
	bool found = false;
	for (int i = m_operations.size() - 1; i >= 0; i--)
	{
		if (m_operations[i].first == operation)
		{
			found = true;
		}

		if (mode == Below)
		{
			if (found)
			{
				return;
			}

			removeOperation(m_operations[i].first);
		}
		else if (mode == Above && found && operation != m_operations[i].first)
		{
			removeOperation(m_operations[i].first);
		}
	}

	calculatePreview();
}

void ProcessingPipeline::setInputImageInUI()
{
	if (m_inputView) // if view already created, replace the image and recalculate
	{
		m_inputView->setImage(m_image);
		calculatePreview();
	}
	else
	{
		auto label = new AdvancedLabel();
		label->setImage(m_image);
		label->setAlignment(Qt::AlignCenter);
		m_inputView = label;
		auto frame = addUIElement(label, Qt::blue);
		frame->setFixedSize(QSize(170, 170));

		connect(label, &AdvancedLabel::doubleClicked, [=]() { showProcessingOutcome(m_image); });
		connect(frame, &AdvancedFrame::doubleClicked, [=]() { showProcessingOutcome(m_image); });
		connect(label, &AdvancedLabel::clicked, [=](auto& pos, auto btn) { if (m_imageSelectionActive && btn == Qt::LeftButton) emit imageSelected(m_image); });
		connect(label, &AdvancedLabel::mouseEntered, this, [=]() { if (m_imageSelectionActive) label->grayOut(false); });
		connect(label, &AdvancedLabel::mouseLeaved, this, [=]() { if (m_imageSelectionActive) label->grayOut(true); });
	}
}

void ProcessingPipeline::addOperationInUI(Operation* operation)
{
	if (m_image.isNull() || operation == nullptr)
	{
		return;
	}

	auto container = new QWidget();
	auto layout = new QVBoxLayout();
	auto label = new AdvancedLabel();
	auto preview = new AdvancedLabel();
	auto line = new AdvancedFrame();
	auto cont = operation->widget();

	layout->setSpacing(0);
	layout->setMargin(0);

	label->setAlignment(Qt::AlignCenter);
	label->setText(operation->getName());

	line->setFrameShape(AdvancedFrame::Shape::HLine);
	line->setStyleSheet(".AdvancedFrame { background-color: black; }");
	preview->setStyleSheet(".AdvancedLabel { background-color: blue; }");

	layout->addWidget(label);
	layout->addWidget(line);

	if (cont) layout->addWidget(cont);

	container->setLayout(layout);

	auto arrow = addArrowItem();
	auto frame = addUIElement(container, QColor(Qt::white));
	auto arrow2 = addArrowItem();
	auto prev_frame = addUIElement(preview, QColor(Qt::blue));

	prev_frame->setFixedSize(QSize(170, 170));


	frame->setContextMenu(menuForOperation(operation));
	label->setContextMenu(menuForOperation(operation));

	m_operations.push_back({operation, {preview, { arrow, frame, arrow2, prev_frame }}});

	frame->setObjectName("operation");
	label->setDataSuffix("operation");
	label->setDraggable(true);
	frame->setDataSuffix("operation");
	frame->setDraggable(true);

	connect(label, &AdvancedLabel::dragAccepted, std::bind(&ProcessingPipeline::removeOperation, this, operation));
	connect(frame, &AdvancedFrame::dragAccepted, std::bind(&ProcessingPipeline::removeOperation, this, operation));
	connect(label, &AdvancedLabel::doubleClicked, std::bind(&ProcessingPipeline::removeOperation, this, operation));
	connect(frame, &AdvancedFrame::doubleClicked, std::bind(&ProcessingPipeline::removeOperation, this, operation));
	connect(preview, &AdvancedLabel::doubleClicked, std::bind(&ProcessingPipeline::showResult, this, operation));
	connect(prev_frame, &AdvancedFrame::doubleClicked, std::bind(&ProcessingPipeline::showResult, this, operation));
	connect(preview, &AdvancedLabel::clicked, [=](auto& pos, auto btn) { if (m_imageSelectionActive && btn == Qt::LeftButton) selectResult(operation); });
	connect(preview, &AdvancedLabel::mouseEntered, this, [=]() { if (m_imageSelectionActive) preview->grayOut(false); });
	connect(preview, &AdvancedLabel::mouseLeaved, this, [=]() { if (m_imageSelectionActive) preview->grayOut(true); });
}

void ProcessingPipeline::insertOperationInUI(Operation* operation, int index)
{
	QList<AdvancedFrame*> list;
	decltype(m_operations) list2;

	for (auto i = index; i < m_operations.size(); i++)
	{
		for (const auto& frame : m_operations[i].second.second)
		{
			list.push_back(frame);
		}

		list2.push_back(m_operations[i]);
	}

	for (auto& item : list2)
	{
		m_operations.removeAll(item);
	}

	for (auto& frame : list)
	{
		m_activeLayout->layout()->removeWidget(frame);
	}

	addOperationInUI(operation);

	for (auto& frame : list)
	{
		m_activeLayout->layout()->addWidget(frame);
		m_activeLayout->layout()->setAlignment(frame, Qt::AlignHCenter);
	}

	for (auto& item : list2)
	{
		m_operations.push_back(item);
	}
}

AdvancedFrame* ProcessingPipeline::addArrowItem()
{
	auto label = new AdvancedLabel();

	if (m_orientation == Qt::Vertical)
		label->setText(QString(QChar(0x25BC)));
	else
		label->setText(QString(QChar(0x25BA)));

	auto font = label->font();
	font.setPointSize(30);
	label->setFont(font);
	auto frame = addUIElement(label, QColor(0, 0, 0, 0));
	frame->setObjectName("arrow");
	return frame;
}

AdvancedFrame* ProcessingPipeline::addUIElement(QWidget* widget, const QColor& frameColor)
{
	auto frame = new AdvancedFrame();
	frame->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	frame->setStyleSheet(QString(".AdvancedFrame { background-color: #%1; border: 1px solid black; border-radius: 10px}").arg(frameColor.rgba(), 8, 16));
	auto layout = new QGridLayout();
   layout->addWidget(widget);
   layout->setAlignment(widget, Qt::AlignCenter);
	frame->setLayout(layout);

	m_activeLayout->layout()->addWidget(frame);
	m_activeLayout->layout()->setAlignment(frame, Qt::AlignCenter);
	return frame;
}

void ProcessingPipeline::removeUIElement(QWidget* widget)
{
	m_activeLayout->layout()->removeWidget(widget);
	widget->deleteLater();
}

void ProcessingPipeline::resetUI()
{
	for (auto i = 0; i < m_activeLayout->layout()->count(); i++)
	{
		auto item = m_activeLayout->layout()->takeAt(i);
		if (item)
		{
			delete item;
		}
	}
}

void ProcessingPipeline::calculatePreview()
{
	if (!m_operations.size())
	{
		return;
	}

	if (m_isRealProcessing)
	{
		m_operations.first().first->execute(m_image);
	}
	else
	{
		m_operations.first().first->execute(m_image.scaled(QSize(150, 150), Qt::KeepAspectRatio));
	}
}

void ProcessingPipeline::selectResult(Operation* operation)
{
	m_cancelOperation = operation;
	m_isRealProcessing = true;
	connect(this, &ProcessingPipeline::processingDone, this, &ProcessingPipeline::selectProcessingOutcome);
	// start execution
	m_operations.first().first->execute(m_image);
}

void ProcessingPipeline::showImageForOperation(Operation* operation, const ImageData& image)
{
	for (auto& item : m_operations)
	{
		if (item.first == operation)
		{
			item.second.first->setImage(image);
		}
	}
}

Operation* ProcessingPipeline::operationAtPos(const QPoint& pos, int* index /*= nullptr*/)
{
	int tmp = 0;
	if (index == nullptr) index = &tmp;
	*index = 0;

   for (const auto& operation : m_operations)
   {
      for (const auto& widget : operation.second.second)
      {
         if (widget->objectName() == "operation" && widget->rect().contains(widget->mapFromGlobal(pos)))
         {
				return operation.first;
         }
      }

      (*index)++;
   }

	return nullptr;
}

void ProcessingPipeline::saveProcessingOutcome(const QImage& image)
{
	m_cancelOperation = nullptr;
	m_isRealProcessing = false;
	disconnect(this, &ProcessingPipeline::processingDone, this, &ProcessingPipeline::saveProcessingOutcome);

	image.save(QString("%1/%2.png").arg(m_outputPath).arg(m_currentIndex), "PNG");
}

void ProcessingPipeline::showProcessingOutcome(const QImage& image)
{
	auto width = image.width();
	m_cancelOperation = nullptr;
	m_isRealProcessing = false;
	disconnect(this, &ProcessingPipeline::processingDone, this, &ProcessingPipeline::showProcessingOutcome);

	auto diag = new QDialog(this, Qt::Window);
	diag->setWindowTitle("Image Preview");
	diag->setLayout(new QGridLayout());
	diag->setWindowFlags(diag->windowFlags() & ~Qt::WindowContextHelpButtonHint);

	auto label = new AdvancedLabel();
	auto menu = new QMenu(diag);
	auto save = new QAction("Save");

	connect(save, &QAction::triggered, [=]()
	{
		QString path = QFileDialog::getSaveFileName(diag, "where to save the image?", QString(), "Image (*.png)");
		if (path.isEmpty()) return;

		if (!image.save(path, "PNG"))
			QMessageBox::critical(this, "error", "the image could not be saved!");

		QMessageBox::information(this, "saved", "the image has been saved!");
	});

	connect(label, &AdvancedLabel::doubleClicked, diag, &QDialog::close);

	menu->addAction(save);
	label->setContextMenu(menu);

	diag->layout()->addWidget(label);
	diag->layout()->setAlignment(label, Qt::AlignCenter);


	auto screen = QApplication::primaryScreen()->geometry();
	if (image.width() > screen.width() * 0.75
		 || image.height() > screen.height() * 0.75)
	{
		label->setImage(image.scaled(screen.size() * 0.95, Qt::KeepAspectRatio));
		diag->setWindowState(diag->windowState() | Qt::WindowMaximized);
	}
	else if (image.width() < 150 && image.height() < 150)
	{
		label->setImage(image.scaled(QSize(150, 150), Qt::KeepAspectRatio));
	}
	else
	{
		label->setImage(image);
	}

	label->setFullResolution(true);
	diag->exec();
}

void ProcessingPipeline::selectProcessingOutcome(const QImage& image)
{
	m_cancelOperation = nullptr;
	m_isRealProcessing = false;
	disconnect(this, &ProcessingPipeline::processingDone, this, &ProcessingPipeline::selectProcessingOutcome);

	emit imageSelected(image);
}

void ProcessingPipeline::showResult(Operation* operation)
{
	m_cancelOperation = operation;
	m_isRealProcessing = true;
	connect(this, &ProcessingPipeline::processingDone, this, &ProcessingPipeline::showProcessingOutcome);
	// start execution
	//auto width = m_image.width();
	m_operations.first().first->execute(m_image);
}

void ProcessingPipeline::reconnectAllOperations()
{
	int index = 0;
	for (const auto& kvp : m_operations)
	{
		const auto& operation = kvp.first;
		operation->disconnect();
		connect(operation, &Operation::imageReady, [=](auto& image) { imageProcessed(index, image); });
		connect(operation, &Operation::updated, this, &ProcessingPipeline::calculatePreview);
		index++;
	}
}

QMenu* ProcessingPipeline::menuForOperation(Operation* operation)
{
	auto result = new QMenu(this);

	auto action1 = new QAction("Remove");
	auto action2 = new QAction("Remove all");
	auto action3 = new QAction("Remove all above");
	auto action4 = new QAction("Remove all below");

	connect(action1, &QAction::triggered, std::bind(&ProcessingPipeline::removeOperation, this, operation));
	connect(action2, &QAction::triggered, this, &ProcessingPipeline::removeAllOperations);
	connect(action3, &QAction::triggered, std::bind(&ProcessingPipeline::removeOperationMode, this, Mode::Above, operation));
	connect(action4, &QAction::triggered, std::bind(&ProcessingPipeline::removeOperationMode, this, Mode::Below, operation));

	result->addAction(action1);
	result->addAction(action2);
	result->addAction(action3);
	result->addAction(action4);

	return result;
}
