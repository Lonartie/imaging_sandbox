#pragma once

#include "stdafx.h"
#include "Interpolation.h"

#include <vector>

class InterpolationRegistry
{
public:
	template<typename T>
	bool registerInterpolation();

	std::vector<std::shared_ptr<Interpolation>> getInterpolations();

	static InterpolationRegistry& instance();

private:

	InterpolationRegistry() = default;
	std::vector<std::function<std::shared_ptr<Interpolation>()>> m_interpolations;
};



template<typename T>
bool InterpolationRegistry::registerInterpolation()
{
	InterpolationRegistry::m_interpolations.push_back([]() { return std::make_unique<T>(); });
	return true;
}
