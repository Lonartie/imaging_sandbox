#pragma once

#include "stdafx.h"
#include "CommonMacros.h"
#include "OperationParameter.h"
#include "ImageData.h"
#include "ImageType.h"

class Operation : public QObject
{
   Q_OBJECT

public:

	Operation();
	Operation(Operation&& op) = delete;
	Operation(const Operation&) = delete;
   virtual ~Operation();

public:

   QWidget* widget(QWidget* parent = Q_NULLPTR) const;

public slots:

   void execute(const ImageData& image);
	void emit_updated();

signals:

   void imageReady(const ImageData& image);
	void updated() const;

public /*abstract functions*/:

   virtual ImageData processImage(const ImageData& image) const = 0;
   virtual QString getName() const = 0;
   virtual Operation* copy() const = 0;
   virtual ImageType getImageType() const = 0;
	
protected:

	std::vector<OperationParameter*> parameters;

};
