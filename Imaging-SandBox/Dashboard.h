#pragma once

#include <QtWidgets/QWidget>
#include "ui_Dashboard.h"
#include "ImageType.h"

class Dashboard: public QWidget
{
	Q_OBJECT

public:
	Dashboard(QWidget *parent = Q_NULLPTR);
	~Dashboard();

	static Dashboard& instance();

public slots:

	void addPipeline();
	void removePipeline(ProcessingPipeline* pipeline);

	void loadImages();
	void runProcessingPipelines();

	void removeAllOperations();

	void setPipelineOrientation(Qt::Orientation orientation);
	void rotate();

	void imageSelectionRequested(std::size_t id);
	void imageHasBeenSelected(const QImage& image);
   void cancelImageSelection();

signals:

	void imageSelected(std::size_t id, const QImage& image) const;

private slots:

	void sendImageSelectionSignal(bool on);

protected:

	virtual bool eventFilter(QObject *watched, QEvent *event) override;

private:

	static Dashboard* __instance;

	std::size_t m_currentSelectorID;
	QBoxLayout * m_activeLayout = nullptr;
	Qt::Orientation m_orientation;
	Ui::DashboardClass m_ui;
};
