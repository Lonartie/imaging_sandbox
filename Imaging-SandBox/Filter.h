#pragma once

#include "stdafx.h"

class Filter
{
public:

   enum ColorChannel
   { Red, Green, Blue, Alpha, GrayScale };

   static QImage applyFilter(const QImage& image, const std::vector<std::vector<float>>& matrix, float multiplicator);
   static QColor applyFilter(const QImage& image, const QPoint& position, const std::vector<std::vector<float>>& matrix, float multiplicator);
   static float applyFilter(const QImage& image, const QPoint& position, const std::vector<std::vector<float>>& matrix, float multiplicator, ColorChannel channel);

};
