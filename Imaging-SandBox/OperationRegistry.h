#pragma once

#include "stdafx.h"
#include "Operation.h"

#include <vector>

class OperationRegistry
{
public:
	template<typename T>
	bool registerOperation();

	std::vector<std::shared_ptr<Operation>> getOperations();

	static OperationRegistry& instance();

private:

	OperationRegistry() = default;
	std::vector<std::function<std::shared_ptr<Operation>()>> m_operations;
};



template<typename T>
bool OperationRegistry::registerOperation()
{
	OperationRegistry::m_operations.push_back([]() { return std::make_shared<T>(); });
	return true;
}
