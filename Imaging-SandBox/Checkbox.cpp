#include "stdafx.h"
#include "Checkbox.h"

Checkbox::Checkbox(const QString& label, bool* connected)
	: m_text(label)
	, m_connected(connected)
{
	if (m_connected)
		*m_connected = false;
}

QWidget* Checkbox::widget(QWidget* parent /*= Q_NULLPTR*/)
{
	auto container = new QWidget(parent);
	auto layout = new QHBoxLayout();
	auto box = new QCheckBox();

	layout->setSpacing(0);
	layout->setMargin(0);

	box->setText(m_text);
	if (m_connected) box->setChecked(*m_connected);

	connect(box, &QCheckBox::stateChanged, this, [=](int state) 
	{
		if (!m_connected) return;

		*m_connected = state == Qt::CheckState::Checked;
		emit updated();
	});

	layout->addWidget(box);

	container->setLayout(layout);
	return container;
}
