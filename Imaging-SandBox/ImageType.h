#pragma once

enum class ImageType
{
   QImage = (1 << 0),
   FImage = (1 << 1),
};