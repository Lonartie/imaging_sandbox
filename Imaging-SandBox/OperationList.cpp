#include "stdafx.h"
#include "OperationList.h"

extern bool TouchDevice;

OperationList::OperationList(QWidget *parent)
    : QWidget(parent)
{
   m_ui.setupUi(this);
   setAcceptDrops(true);
   m_ui.listWidget->setDragDropMode(QAbstractItemView::InternalMove);
   m_ui.listWidget->setDefaultDropAction(Qt::CopyAction);
   if(TouchDevice) QScroller::grabGesture(m_ui.listWidget, QScroller::LeftMouseButtonGesture);
	connect(m_ui.listWidget, &QListWidget::itemDoubleClicked, [=](auto item) 
	{
		emit operationDoubleClicked((*static_cast<Operation**>(static_cast<void*>(item->data(Qt::UserRole + 1).toByteArray().data())))->copy());
	});
   connect(m_ui.searchEdit, &QLineEdit::textChanged, this, &OperationList::search);

   m_ui.listWidget->setSortingEnabled(true);
}

OperationList::~OperationList()
{
   //for (auto& operation : m_operations)
   //   if (operation)
   //      delete operation;
}

void OperationList::addOperation(Operation* operation)
{
   if (!operation) return;

	operation = operation->copy();
   m_operations.push_back(operation);
   addOperationInUI(operation);
}

void OperationList::imageSelection(bool on)
{
	if (on)
		setStyleSheet("QWidget { background-color: lightgray }");
	else
		setStyleSheet("");
}

void OperationList::dragEnterEvent(QDragEnterEvent* event)
{
   auto formats = event->mimeData()->formats();
   if (event->mimeData()->hasFormat("application/x-dnditemdataoperation"))
      event->acceptProposedAction();
}

void OperationList::dropEvent(QDropEvent* event)
{
   event->acceptProposedAction();
}

void OperationList::addOperationInUI(Operation* operation) const
{
   auto name = operation->getName();
   auto item = new QListWidgetItem(name);
   QByteArray addr(static_cast<const char*>(static_cast<void*>(&operation)), sizeof(Operation*));
   item->setData(Qt::UserRole + 1, addr);
   m_ui.listWidget->addItem(item);
}

void OperationList::search(const QString& text)
{
   m_searchString = text;
   updateVisibilities();
}

void OperationList::updateVisibilities()
{
   for (auto i = 0; i < m_ui.listWidget->model()->rowCount(); i++)
   {
      auto item = m_ui.listWidget->item(i);
      if (!item) continue;
      auto contains = item->text().contains(m_searchString, Qt::CaseInsensitive);
      item->setHidden(!contains);
   }
}
