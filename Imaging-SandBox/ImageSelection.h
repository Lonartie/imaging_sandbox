#pragma once

#include "CommonFunctions.h"

class ImageSelection : public OperationParameter
{
	Q_OBJECT;

public:

	ImageSelection(const QString& text, QImage* connected);
	virtual ~ImageSelection() = default;

	virtual QWidget* widget(QWidget* parent = Q_NULLPTR) override;

signals:

	void imageSelectionRequested(std::size_t id) const;

private slots:

	void selectImage(std::size_t id, const QImage& image) const;

private:

	static std::size_t idCounter;
	std::size_t m_id = 0;
	QString m_text;
	QImage* m_connected = nullptr;
};
