#include "stdafx.h"
#include "BlendImage.h"

BlendImage::BlendImage()
	: m_methods({"average", "addition", "subtraction", "multiplication"})
{
	REGISTER_PARAMETER(new ImageSelection("other source: ", &m_image));
	REGISTER_PARAMETER(new IntegerSlider("percentage: ", 0, 100, 1, &m_value, "%"));
	REGISTER_PARAMETER(new ComboBox("blending method: ", m_methods, &m_method));
}

ImageData BlendImage::processImage(const ImageData& imageData) const
{
	if (m_image.isNull()) return QImage();
	if (!imageData.isQImage()) return QImage();
	const QImage& image = imageData.getQImage();

	int width = std::min(image.width(), m_image.width());
	int height = std::min(image.height(), m_image.height());
	QSize size = QSize(width, height);

	bool is_average = m_method == "average";
	bool is_addition = m_method == "addition";
	bool is_multiplication = m_method == "multiplication";
	bool is_subtraction = m_method == "subtraction";

	auto image_1 = image.scaled(size, Qt::KeepAspectRatio);
	auto image_2 = m_image.scaled(size, Qt::KeepAspectRatio);

	QImage result(width, height, image.format());

	OMP_LOOP_IMAGE(result)
	{
		QColor pix_1 = Qt::transparent, pix_2 = Qt::transparent;

		if (image_1.width() > x && image_1.height() > y)
			pix_1 = image_1.pixelColor(x, y);

		if (image_2.width() > x && image_2.height() > y)
			pix_2 = image_2.pixelColor(x, y);

		if (is_average) result.setPixelColor(x, y, faded(pix_1, pix_2, m_value / 100.0));
		if (is_addition) result.setPixelColor(x, y, faded(pix_1, add(pix_1, pix_2), m_value / 100.0));
		if (is_multiplication) result.setPixelColor(x, y, faded(pix_1, mul(pix_1, pix_2), m_value / 100.0));
		if (is_subtraction) result.setPixelColor(x, y, faded(pix_1, sub(pix_1, pix_2), m_value / 100.0));
	}

	return result;
}

ImageType BlendImage::getImageType() const
{
	return ImageType::QImage;
}

QColor BlendImage::faded(const QColor& a, const QColor& b, float value) const
{
	return QColor
	(
		a.red() + ((b.red() - a.red()) * value),
		a.green() + ((b.green() - a.green()) * value),
		a.blue() + ((b.blue() - a.blue()) * value),
		a.alpha() + ((b.alpha() - a.alpha()) * value)
	);
}

QColor BlendImage::add(const QColor& a, const QColor& b) const
{
	return QColor
	(
		std::min(255, a.red() + b.red()),
		std::min(255, a.green() + b.green()),
		std::min(255, a.blue() + b.blue()),
		std::min(255, a.alpha() + b.alpha())
	);
}

QColor BlendImage::sub(const QColor& a, const QColor& b) const
{
	return QColor
	(
		std::max(0, a.red() - b.red()),
		std::max(0, a.green() - b.green()),
		std::max(0, a.blue() - b.blue()),
		std::max(0, a.alpha() - b.alpha())
	);
}

QColor BlendImage::mul(const QColor& a, const QColor& b) const
{
	return QColor
	(
		255 * (a.red() / 255.0 * b.red() / 255.0),
		255 * (a.green() / 255.0 * b.green() / 255.0),
		255 * (a.blue() / 255.0 * b.blue() / 255.0),
		255 * (a.alpha() / 255.0 * b.alpha() / 255.0)
	);
}

REGISTER_OPERATION(BlendImage);