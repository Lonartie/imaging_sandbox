#pragma once

#include "stdafx.h"
#include "ui_ProcessingPipeline.h"
#include "ImageView.h"
#include "Operation.h"
#include "AdvancedLabel.h"
#include "AdvancedFrame.h"

class ProcessingPipeline: public QWidget
{
   Q_OBJECT

public /*ctors*/:

   ProcessingPipeline(QWidget* parent = Q_NULLPTR);
   ~ProcessingPipeline();

public slots /*exposed functions*/:

	void imageProcessed(int operationIndex, const ImageData& image);
	void setImage(const QImage& image);
	void addOperation(Operation* operation);
	void insertOperation(Operation* operation, int index);
	void removeOperation(Operation* operation);
   void process(const QString& dir, int index);
   void removeAllOperations();
	void setOrientation(Qt::Orientation orientation);
	void imageSelection(bool on);

signals:

	void addNewInstance();
	void removeInstance();
   void removeAllOperationsFromAllPipelines();
   void processingDone(const ImageData& image) const;
	void imageSelected(const QImage& image) const;

protected /*inherited functions*/:

   void dragEnterEvent(QDragEnterEvent* event) override;
   void dropEvent(QDropEvent* event) override;
   void mousePressEvent(QMouseEvent* event) override;

private /*ui functions*/:

   void setInputImageInUI();
	void addOperationInUI(Operation* operation);
	void insertOperationInUI(Operation* operation, int index);
   AdvancedFrame* addArrowItem();
   AdvancedFrame* addUIElement(QWidget* widget, const QColor& frameColor);
   void removeUIElement(QWidget* widget);
   void resetUI();
   void showImageForOperation(Operation* operation, const ImageData& image);
   Operation* operationAtPos(const QPoint& pos, int* index = nullptr);

private slots:

   void saveProcessingOutcome(const QImage& image);
   void showProcessingOutcome(const QImage& image);
	void selectProcessingOutcome(const QImage& image);
	void showResult(Operation* operation);
	void calculatePreview();
	void selectResult(Operation* operation);

private /*functions*/:

   enum Mode { Above, Below };

	void reconnectAllOperations();
   QMenu* menuForOperation(Operation* operation);
   void removeOperationMode(Mode mode, Operation* operation);

private /*members*/:

	bool m_imageSelectionActive = false;
	Qt::Orientation m_orientation = Qt::Vertical;
   bool m_isRealProcessing = false;
   QString m_outputPath;
   int m_currentIndex = 0;
   QImage m_image;
	QBoxLayout* m_activeLayout;
   AdvancedLabel* m_inputView = nullptr;
   Operation* m_cancelOperation = nullptr;
   QVector<QPair<Operation*, QPair<AdvancedLabel*, QList<AdvancedFrame*>>>> m_operations;
   Ui::ProcessingPipeline m_ui;
	static int instance;
};
