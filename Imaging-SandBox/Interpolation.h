#pragma once

#include "stdafx.h"
#include "OperationParameter.h"

class Interpolation: public QObject
{
	Q_OBJECT

public:

	Interpolation();
	Interpolation(Interpolation&& op) = delete;
	Interpolation(const Interpolation&) = delete;
	virtual ~Interpolation() = default;

	QWidget* widget(QWidget* parent = Q_NULLPTR) const;

	virtual QColor interpolate(const QImage& image, const QPointF& required) const = 0;
	virtual QString getName() const = 0;
	virtual Interpolation* copy() const = 0;

signals:

	void updated() const;

protected:
	std::vector<OperationParameter*> parameters;

};
