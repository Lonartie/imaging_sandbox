#pragma once

#include "CommonFunctions.h"

class Tone : public Operation
{
   Q_OBJECT;
   OPERATION(Tone);

public:

   Tone();

   virtual ImageData processImage(const ImageData& image) const override;
   
   virtual ImageType getImageType() const override;

private:

   QColor tone(const QColor& a) const;
   QColor mul(const QColor& a, const QColor& b) const;
   QColor fade(const QColor& a, const QColor& b, float value) const;

   int m_tone = 0;

};
