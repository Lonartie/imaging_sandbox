#include "stdafx.h"
#include "ImageSelection.h"
#include "AdvancedLabel.h"
#include "Dashboard.h"

std::size_t ImageSelection::idCounter = 0;

ImageSelection::ImageSelection(const QString& text, QImage* connected)
	: m_connected(connected)
	, m_id(idCounter++)
	, m_text(text)
{
	if (!m_connected) return;
	*m_connected = QImage();

	connect(&Dashboard::instance(), &Dashboard::imageSelected, this, &ImageSelection::selectImage);
}

QWidget* ImageSelection::widget(QWidget* parent /*= Q_NULLPTR*/)
{
	if (!m_connected) return nullptr;

	auto container = new QWidget(parent);
   auto layout = new QHBoxLayout();
   auto slayout = new QHBoxLayout();
   auto desc = new QLabel();
   auto fram = new AdvancedFrame();
	auto label = new AdvancedLabel(m_connected->scaled(QSize(50, 50), Qt::KeepAspectRatio));

	layout->setSpacing(0);
   layout->setMargin(0);
   slayout->setSpacing(0);
   slayout->setMargin(0);

	slayout->addWidget(label);
	fram->setLayout(slayout);
	
	fram->setStyleSheet(".AdvancedFrame { background-color: lightgray; border: 1px solid black; border-radius: 10px }");
	fram->setFixedSize(60, 60);

	layout->addWidget(desc);
	layout->addWidget(fram);

	desc->setText(m_text);


	label->setFullResolution(true);
	label->setFixedSize(QSize(51, 51));

   connect(label, &AdvancedLabel::clicked, std::bind(&Dashboard::imageSelectionRequested, &Dashboard::instance(), m_id));
   connect(fram, &AdvancedFrame::clicked, std::bind(&Dashboard::imageSelectionRequested, &Dashboard::instance(), m_id));

	connect(&Dashboard::instance(), &Dashboard::imageSelected, label, [=](std::size_t id, auto& image)
	{ 
		if (id != m_id) return;
		label->setImage(image.scaled(QSize(50, 50), Qt::KeepAspectRatio)); 
	});

	container->setLayout(layout);
	return container;
}

void ImageSelection::selectImage(std::size_t id, const QImage& image) const
{
	if (id != m_id) return;

	*m_connected = image;
	emit updated();
}

