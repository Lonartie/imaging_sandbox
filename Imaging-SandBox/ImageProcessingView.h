#pragma once

#include "stdafx.h"
#include "ui_ImageProcessingView.h"

class ImageProcessingView: public QWidget
{
   Q_OBJECT

public /*ctors*/:

   ImageProcessingView(QWidget* parent = Q_NULLPTR);
   ~ImageProcessingView() = default;

public /*exposed functions*/:

   void setImage(const QImage& image);
   void setOperationName(const QString& operation);

public slots:

   void setProgress(int progress);

private /*ui functions*/:

   void setOperationNameInUI() const;
   void setProgressInUI() const;

private /*members*/:
   
   QString m_operation;
   int m_progress;
   Ui::ImageProcessingView m_ui;
};
