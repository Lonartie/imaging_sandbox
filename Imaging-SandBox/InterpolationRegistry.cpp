#include "stdafx.h"
#include "InterpolationRegistry.h"

std::vector<std::shared_ptr<Interpolation>> InterpolationRegistry::getInterpolations()
{
	std::vector<std::shared_ptr<Interpolation>> operations;
	for (auto& operation : m_interpolations)
		operations.push_back(operation());
	return operations;
}

InterpolationRegistry& InterpolationRegistry::instance()
{
	static InterpolationRegistry _instance;
	return _instance;
}

