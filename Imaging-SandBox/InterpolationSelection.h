#pragma once

#include "stdafx.h"
#include "OperationParameter.h"
#include "CommonMacros.h"
#include "Interpolation.h"

class InterpolationSelection: public OperationParameter
{
	Q_OBJECT;
public:

	InterpolationSelection(Interpolation** connected);
	
	virtual QWidget* widget(QWidget* parent = Q_NULLPTR) override;

private slots:

	//void showConfig(QWidget* parent = Q_NULLPTR) const;

private:

	std::vector<std::shared_ptr<Interpolation>> m_interpolations;
	QStringList m_names;
	Interpolation** m_connected;
	QWidget* m_lastWidget = nullptr;
};
