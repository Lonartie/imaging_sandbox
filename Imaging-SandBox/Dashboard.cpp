#include "stdafx.h"
#include "Dashboard.h"
#include "Dialog.h"
#include "ImageProcessingView.h"
#include "Operation.h"
#include "OperationRegistry.h"
#include "CommonMacros.h"
#include "Versioning.h"
#include "OperationParameter.h"
#include "IntegerSlider.h"

Dashboard* Dashboard::__instance = nullptr;

Dashboard::Dashboard(QWidget* parent)
	: QWidget(parent)
{
	__instance = this;
	m_ui.setupUi(this);

	m_activeLayout = m_ui.pipelines;
	m_orientation = Qt::Horizontal;

	connect(m_ui.Images, &ImageList::imageDoubleClicked, m_ui.Pipeline, &ProcessingPipeline::setImage);
	connect(m_ui.Operations, &OperationList::operationDoubleClicked, m_ui.Pipeline, &ProcessingPipeline::addOperation);

	connect(m_ui.Pipeline, &ProcessingPipeline::addNewInstance, this, &Dashboard::addPipeline);
	connect(m_ui.Pipeline, &ProcessingPipeline::removeInstance, std::bind(&Dashboard::removePipeline, this, m_ui.Pipeline));
	connect(m_ui.Pipeline, &ProcessingPipeline::removeAllOperationsFromAllPipelines, this, &Dashboard::removeAllOperations);

	connect(m_ui.loadImages, &QPushButton::pressed, this, &Dashboard::loadImages);
	connect(m_ui.renderPipeline, &QPushButton::pressed, this, &Dashboard::runProcessingPipelines);
	connect(m_ui.rotate, &QPushButton::pressed, this, &Dashboard::rotate);

	setWindowTitle(QString("%1 (%2)").arg(windowTitle()).arg(Versioning::getVersionString()));

#if !defined(NDEBUG)

	QImage placeholder(":/Dashboard/placeholder");
	QImage placeholder2(":/Dashboard/placeholder2");

	//m_ui.Images->addImage(placeholder);
	//m_ui.Images->addImage(placeholder2);
	//m_ui.Images->addImage(placeholder);
	//m_ui.Images->addImage(placeholder2);
	m_ui.Images->addImage(placeholder);

#endif

	for (const auto& operation : OperationRegistry::instance().getOperations())
		m_ui.Operations->addOperation(operation.get());

	installEventFilter(this);
}

Dashboard::~Dashboard()
{
}

Dashboard& Dashboard::instance()
{
	if (!__instance)
		throw std::runtime_error("no instance created yet!");

	return *__instance;
}

void Dashboard::removePipeline(ProcessingPipeline* pipeline)
{
	m_activeLayout->removeWidget(pipeline);
	pipeline->deleteLater();
}

void Dashboard::loadImages()
{
	static auto filter = "Image (*.png *.jpg *.jpeg *.bmp)";
	auto result = QFileDialog::getOpenFileNames(this, "select images to load", QString(), filter);
	if (result.size() == 0) return;

	for (auto& file : result)
	{
		QImage image(file);
		if (!image.isNull())
		{
			m_ui.Images->addImage(image);
		}
	}

	QMessageBox::information(this, "images loaded", "the images were loaded");
}

void Dashboard::runProcessingPipelines()
{
	int index = 0;
	QString outDir = QFileDialog::getExistingDirectory(this, "select an output directory", QString());
	for (auto i = 0; i < m_activeLayout->count(); i++)
	{
		auto pipeline = qobject_cast<ProcessingPipeline*>(m_activeLayout->itemAt(i)->widget());
		pipeline->process(outDir, ++index);
	}
}

void Dashboard::removeAllOperations()
{
	for (auto i = 0; i < m_activeLayout->count(); i++)
	{
		auto pipeline = qobject_cast<ProcessingPipeline*>(m_activeLayout->itemAt(i)->widget());
		pipeline->removeAllOperations();
	}
}

void Dashboard::setPipelineOrientation(Qt::Orientation orientation)
{
	m_ui.gridLayout->removeItem(m_activeLayout);

	QBoxLayout* newLayout;

	if (orientation == Qt::Orientation::Horizontal)
		newLayout = new QHBoxLayout();
	else
		newLayout = new QVBoxLayout();

	newLayout->setAlignment(Qt::AlignCenter);
	newLayout->setSpacing(0);
	newLayout->setMargin(0);
	newLayout->setParent(m_ui.gridLayout);

	for (int i = 0; i < m_activeLayout->count(); i++)
	{
		auto widget = qobject_cast<ProcessingPipeline*>(m_activeLayout->itemAt(i)->widget());
		newLayout->addItem(m_activeLayout->itemAt(i));
		widget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
		widget->setOrientation(orientation == Qt::Vertical ? Qt::Horizontal : Qt::Vertical);
	}

	m_activeLayout = newLayout;
	m_orientation = orientation;
	m_ui.gridLayout->addItem(newLayout, 1, 1, 1, 2);
}

void Dashboard::rotate()
{
	static auto horizontal = QImage(":/Dashboard/run");
	auto center = horizontal.rect().center();
	QMatrix matrix;
	matrix.translate(center.x(), center.y());
	matrix.rotate(90);
	static auto vertical = horizontal.transformed(matrix);

	if (m_orientation == Qt::Vertical)
	{
		setPipelineOrientation(Qt::Horizontal);
		m_ui.rotate->setIcon(QIcon(QPixmap::fromImage(horizontal)));
	}
	else
	{
		setPipelineOrientation(Qt::Vertical);
		m_ui.rotate->setIcon(QIcon(QPixmap::fromImage(vertical)));
	}
}

void Dashboard::imageSelectionRequested(std::size_t id)
{
	m_currentSelectorID = id;
	sendImageSelectionSignal(true);
}

void Dashboard::imageHasBeenSelected(const QImage& image)
{
	sendImageSelectionSignal(false);
	emit imageSelected(m_currentSelectorID, image);
}

void Dashboard::cancelImageSelection()
{
	sendImageSelectionSignal(false);
}

void Dashboard::sendImageSelectionSignal(bool on)
{
	if (on)
	{
		setStyleSheet("QWidget#DashboardClass { background-color: lightgray; } .QWidget { background-color: lightgray; }");
		connect(m_ui.Images, &ImageList::imageSelected, this, &Dashboard::imageHasBeenSelected, Qt::QueuedConnection);

		for (int i = 0; i < m_activeLayout->count(); i++)
		{
			auto pipeline = qobject_cast<ProcessingPipeline*>(m_activeLayout->itemAt(i)->widget());
			connect(pipeline, &ProcessingPipeline::imageSelected, this, &Dashboard::imageHasBeenSelected, Qt::QueuedConnection);
		}
	}
	else
	{
		setStyleSheet("");
		disconnect(m_ui.Images, &ImageList::imageSelected, this, &Dashboard::imageHasBeenSelected);

		for (int i = 0; i < m_activeLayout->count(); i++)
		{
			auto pipeline = qobject_cast<ProcessingPipeline*>(m_activeLayout->itemAt(i)->widget());
			disconnect(pipeline, &ProcessingPipeline::imageSelected, this, &Dashboard::imageHasBeenSelected);
		}
	}

	m_ui.Images->imageSelection(on);
	m_ui.Operations->imageSelection(on);

	for (int i = 0; i < m_activeLayout->count(); i++)
	{
		auto pipeline = qobject_cast<ProcessingPipeline*>(m_activeLayout->itemAt(i)->widget());
		pipeline->imageSelection(on);
	}
}

bool Dashboard::eventFilter(QObject *watched, QEvent *event)
{
	if (event->type() == QEvent::Type::KeyPress)
	{
		auto kevent = static_cast<QKeyEvent*>(event);
		if (kevent->key() == Qt::Key_Escape)
		{
			cancelImageSelection();
			return true;
		}
	}
	return QObject::eventFilter(watched, event);
}

void Dashboard::addPipeline()
{
	if (m_activeLayout->count() >= 6) return;

	auto pipeline = new ProcessingPipeline();

	connect(m_ui.Images, &ImageList::imageDoubleClicked, pipeline, &ProcessingPipeline::setImage);
	connect(m_ui.Operations, &OperationList::operationDoubleClicked, pipeline, &ProcessingPipeline::addOperation);

	connect(pipeline, &ProcessingPipeline::addNewInstance, this, &Dashboard::addPipeline);
	connect(pipeline, &ProcessingPipeline::removeInstance, std::bind(&Dashboard::removePipeline, this, pipeline));
	connect(pipeline, &ProcessingPipeline::removeAllOperationsFromAllPipelines, this, &Dashboard::removeAllOperations);

	pipeline->setOrientation(m_orientation == Qt::Vertical ? Qt::Horizontal : Qt::Vertical);
	m_activeLayout->addWidget(pipeline);
}