#include "stdafx.h"
#include "CLMath.h"

FUNCTION_CLASS_CPP_BEGIN(CLMath);

FUNCTION_CPP(CLMath, index_to_x, int, (int, index, int, width),
				 {
					 return index % width;
				 });

FUNCTION_CPP(CLMath, index_to_y, int, (int, index, int, width),
				 {
					 return index / width;
				 });

FUNCTION_CPP(CLMath, xy_to_index, int, (int, x, int, y, int, width),
				 {
					 return y * width + x;
				 });

FUNCTION_CPP(CLMath, clamp_between, float, (float, min, float, max, float, val),
				 {
					 if (val < min) return min;
					 if (val > max) return max;
					 return val;
				 });


FUNCTION_CPP(CLMath, gaussian_value, float, (float, x, float, y, float, sig),
				 {
					 return 1.0f / (2.0f * 3.141592653589796 * sig * sig) * std::exp(-1.0f * (x * x + y * y) / (2.0f * sig * sig));
				 });

FUNCTION_CLASS_CPP_END();