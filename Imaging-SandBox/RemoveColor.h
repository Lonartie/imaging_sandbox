#pragma once

#include "stdafx.h"
#include "CommonFunctions.h"

class RemoveColor : public Operation
{
	Q_OBJECT;
	OPERATION(RemoveColor);

public:
	RemoveColor();
	virtual ~RemoveColor() = default;

	virtual ImageData processImage(const ImageData& imageData) const override;

	virtual ImageType getImageType() const override;

private:

	QColor removeSelected(const QColor& col) const;
	QColor removeOthers(const QColor& col) const;
	bool isInTolerance(const QColor& col) const;
	double distance(const QColor& a, const QColor& b) const;
	QColor fade(const QColor& a, const QColor& b, float value) const;

	static const QString remove_selected;
	static const QString remove_others;

	static const QString hard_removal;
	static const QString soft_removal;

	QString m_removal;
	QString m_type;
	QColor m_color;
	int m_tolerance = 5;

};
