#pragma once

#include "CommonFunctions.h"

class Vignette : public Operation
{
   Q_OBJECT;
   OPERATION(Vignette);

public:

   Vignette();

   virtual ImageData processImage(const ImageData& image) const override;
   
   virtual ImageType getImageType() const override;

private:

   QColor vignette(const QSize& size, const QPoint& pos, const QColor& col) const;

   QColor faded(const QColor& a, const QColor& b, float value) const;

   int m_radius = 80;
   int m_fade = 150;
};
