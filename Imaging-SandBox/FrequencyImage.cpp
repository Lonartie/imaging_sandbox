#include "stdafx.h"
#include "FrequencyImage.h"
#include "CommonMacros.h"

Q_DECLARE_METATYPE(FrequencyImage);

FrequencyImage::FrequencyImage(const QSize& size)
	: _width(size.width())
	, _height(size.height())
{
	malloc();
}

FrequencyImage::FrequencyImage(std::size_t width, std::size_t height)
	: _width(width)
	, _height(height)
{
	malloc();
}

FrequencyImage::FrequencyImage(const QImage& image)
{
	operator=(image);
}

FrequencyImage::FrequencyImage(fftw_complex* data[3], std::size_t width, std::size_t height)
	: _width(width)
	, _height(height)
{
	r = data[0];
	g = data[1];
	b = data[2];
}

FrequencyImage::FrequencyImage(const FrequencyImage& o)
{
	operator=(o);
}

FrequencyImage::FrequencyImage(FrequencyImage&& o)
{
	operator=(std::move(o));
}

FrequencyImage::~FrequencyImage()
{
	clear();
}

FrequencyImage& FrequencyImage::operator=(FrequencyImage&& o) noexcept
{
	clear();

	o.moved = true;
	_width = o._width;
	_height = o._height;

	r = o.r;
	g = o.g;
	b = o.b;

	return *this;
}

FrequencyImage& FrequencyImage::operator=(const FrequencyImage& o) noexcept
{
	clear();

	_width = o._width;
	_height = o._height;

	malloc();

	for (std::size_t i = 0; i < _width * _height; i++)
	{
		r[i][0] = o.r[i][0];
		r[i][1] = o.r[i][1];

		g[i][0] = o.g[i][0];
		g[i][1] = o.g[i][1];

		b[i][0] = o.b[i][0];
		b[i][1] = o.b[i][1];
	}

	return *this;
}

FrequencyImage& FrequencyImage::operator=(const QImage& o) noexcept
{
	clear();

	_width = o.width();
	_height = o.height();

	malloc();

	OMP_LOOP_IMAGE(o)
	{
		auto index = toIndex(x, y);
		auto pix = o.pixelColor(x, y);

		r[index][0] = pix.red() / 255.0;
		g[index][0] = pix.green() / 255.0;
		b[index][0] = pix.blue() / 255.0;
	}

	return *this;
}

FrequencyImage::PixelValue FrequencyImage::getPixel(const QPoint& position)
{
	PixelValue res;

	res.red[0] = r[toIndex(position.x(), position.y())][0];
	res.red[1] = r[toIndex(position.x(), position.y())][1];

	res.green[0] = g[toIndex(position.x(), position.y())][0];
	res.green[1] = g[toIndex(position.x(), position.y())][1];

	res.blue[0] = b[toIndex(position.x(), position.y())][0];
	res.blue[1] = b[toIndex(position.x(), position.y())][1];

	return res;
}

void FrequencyImage::setPixel(const QPoint& position, const PixelValue& value)
{
	r[toIndex(position.x(), position.y())][0] = value.red[0];
	r[toIndex(position.x(), position.y())][1] = value.red[1];

	g[toIndex(position.x(), position.y())][0] = value.green[0];
	g[toIndex(position.x(), position.y())][1] = value.green[1];

	b[toIndex(position.x(), position.y())][0] = value.blue[0];
	b[toIndex(position.x(), position.y())][1] = value.blue[1];
}

fftw_complex* FrequencyImage::getChannel(Filter::ColorChannel channel) const
{
	if (channel == Filter::Red) return r;
	if (channel == Filter::Green) return g;
	if (channel == Filter::Blue) return b;
	return nullptr;
}

void FrequencyImage::clear()
{
	_width = 0;
	_height = 0;

	if (moved) return;

	if (r) fftw_free(r);
	if (g) fftw_free(g);
	if (b) fftw_free(b);
}

void FrequencyImage::malloc()
{
	auto length = _width * _height;

	r = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex) * length));
	g = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex) * length));
	b = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex) * length));
}

QImage FrequencyImage::toImage() const
{
	QImage result(_width, _height, QImage::Format_ARGB32);

	OMP_LOOP_IMAGE(result)
	{
		auto index = toIndex(x, y);

		int _r = std::max(0.0, std::min(255.0, r[index][0] / (_width * _height) * 255));
		int _g = std::max(0.0, std::min(255.0, g[index][0] / (_width * _height) * 255));
		int _b = std::max(0.0, std::min(255.0, b[index][0] / (_width * _height) * 255));

		result.setPixelColor(x, y, QColor(_r, _g, _b));
	}

	return result;
}

QImage FrequencyImage::asImage() const
{
	QImage result(_width, _height, QImage::Format_ARGB32);

	OMP_LOOP_IMAGE(result)
	{
		auto index = toIndex(x, y);

		int _r = std::max(0.0, std::min(255.0, r[index][0] * 64 + r[index][1] * 64));
		int _g = std::max(0.0, std::min(255.0, g[index][0] * 64 + g[index][1] * 64));
		int _b = std::max(0.0, std::min(255.0, b[index][0] * 64 + b[index][1] * 64));

		result.setPixelColor(x, y, QColor(_r, _g, _b));
	}

	return result;
}

std::size_t FrequencyImage::width() const
{
	return _width;
}

std::size_t FrequencyImage::height() const
{
	return _height;
}

FrequencyImage FrequencyImage::fromImage(const QImage& image)
{
	return FrequencyImage(image);
}

FrequencyImage FrequencyImage::fromFrequencies(fftw_complex* data[3], std::size_t width, std::size_t height)
{
	return FrequencyImage(data, width, height);
}

std::size_t FrequencyImage::toIndex(std::size_t x, std::size_t y) const
{
	return y * _width + x;
}

QPoint FrequencyImage::toPoint(std::size_t index) const
{
	return QPoint(index % _width, index / _width);
}
