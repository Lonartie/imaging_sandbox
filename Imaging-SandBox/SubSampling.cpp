#include "stdafx.h"
#include "SubSampling.h"

SubSampling::SubSampling()
{
	REGISTER_PARAMETER(new IntegerSlider("percentage: ", 0, 100, 1, &value, "%"));
	REGISTER_PARAMETER(new InterpolationSelection(&m_interpolation));
}

ImageData SubSampling::processImage(const ImageData& imageData) const
{
	if (!imageData.isQImage()) return QImage();
	const QImage& image = imageData.getQImage();

	auto _value = (100 - value) / 100.0;

	int nwidth = image.width() * _value;
	int nheight = image.height() * _value;

	QImage result(nwidth, nheight, image.format());

	OMP_LOOP_IMAGE(result)
	{
		float nx = x / _value;
		float ny = y / _value;

		result.setPixelColor(x, y, m_interpolation->interpolate(image, QPointF(nx, ny)));
	}

	return result;
}

ImageType SubSampling::getImageType() const
{
	return ImageType::QImage;
}

REGISTER_OPERATION(SubSampling);