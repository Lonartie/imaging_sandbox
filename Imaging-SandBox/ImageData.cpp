#include "stdafx.h"
#include "ImageData.h"

Q_DECLARE_METATYPE(ImageData);

ImageData::ImageData(const QImage& image)
	: is_qimage(true)
	, is_fimage(false)
	, qimage(image)
{}

ImageData::ImageData(const FrequencyImage& image)
	: is_fimage(true)
	, is_qimage(false)
	, fimage(image)
	, asqimage(image.asImage())
{}

ImageData::operator QImage&()
{
	if (is_qimage) return getQImage();
	return asQImage();
}

ImageData::operator const QImage&() const
{
	if (is_qimage) return getQImage();
	return asQImage();
}

ImageData::operator FrequencyImage&()
{
	return fimage;
}

ImageData::operator const FrequencyImage&() const
{
	return fimage;
}

bool ImageData::isQImage() const
{
	return is_qimage;
}

bool ImageData::isFreqImage() const
{
	return is_fimage;
}

const QImage& ImageData::getQImage() const
{
	return qimage;
}

QImage& ImageData::getQImage()
{
	return qimage;
}

const FrequencyImage& ImageData::getFreqImage() const
{
	return fimage;
}

FrequencyImage& ImageData::getFreqImage()
{
	return fimage;
}

const QImage& ImageData::asQImage() const
{
	return asqimage;
}

QImage& ImageData::asQImage()
{
	return asqimage;
}

std::size_t ImageData::width() const
{
	if (is_qimage) return qimage.width();
	if (is_fimage) return fimage.width();
	return 0;
}

std::size_t ImageData::height() const
{
	if (is_qimage) return qimage.height();
	if (is_fimage) return fimage.height();
	return 0;
}

