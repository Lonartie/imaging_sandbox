#include "stdafx.h"
#include "ImageProcessingView.h"

ImageProcessingView::ImageProcessingView(QWidget *parent)
    : QWidget(parent)
{
    m_ui.setupUi(this);
}

void ImageProcessingView::setImage(const QImage& image) 
{
   m_ui.Image->setImage(image);
}

void ImageProcessingView::setOperationName(const QString& operation)
{
   m_operation = operation;
   setOperationNameInUI();
}

void ImageProcessingView::setProgress(int progress)
{
   m_progress = progress;
   setProgressInUI();
}

void ImageProcessingView::setOperationNameInUI() const
{
   m_ui.Operation->setText(m_operation);
}

void ImageProcessingView::setProgressInUI() const
{
   m_ui.Progress->setValue(m_progress);
}
