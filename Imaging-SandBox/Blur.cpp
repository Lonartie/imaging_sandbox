#include "stdafx.h"
#include "Blur.h"
#include "FilterCl.h"

Blur::Blur()
{
	REGISTER_PARAMETER(new IntegerSlider("blur level: ", 1, 10, 1, &m_intensity));
	REGISTER_PARAMETER(new IntegerSlider("iterations: ", 1, 10, 1, &m_iterations));
	REGISTER_PARAMETER(new Checkbox(QString("GPU calculation [%1]").arg(EasyOCL::OCLDevice::GPU().getName()), &m_ocl));
	m_ocl = true;
	if (!FilterCl::compile(&FilterCl::applyFilter, EasyOCL::OCLDevice::GPU()))
	{
		auto program = FilterCl::getProgram(&FilterCl::applyFilter, EasyOCL::OCLDevice::GPU());
		__debugbreak();
	}
}

ImageData Blur::processImage(const ImageData& imageData) const
{
	if (!imageData.isQImage()) return QImage();
	const QImage& image = imageData.getQImage();

	std::size_t size = image.width() * image.height();
	std::vector<int> image_size = {image.width(), image.height()};
	std::vector<int> kernel_size = {1 + 2 * m_intensity, 1 + 2 * m_intensity};
	auto blurMatrix = createBlurMatrix();
	std::vector<std::vector<float>> kernel;
	auto xyToIndex = [&kernel_size](int x, int y) { return y * kernel_size[0] + x; };
	for (int x = 0; x < kernel_size[0]; x++)
	{
		kernel.push_back({});
		for (int y = 0; y < kernel_size[0]; y++)
			kernel[x].push_back(blurMatrix[xyToIndex(x, y)]);
	}

	QImage result = image;
	for (int i = 0; i < m_iterations; i++)
	{
		if (m_ocl)
		{
			auto raw = result.bits();
			if (FilterCl::applyFilter(EasyOCL::OCLDevice::GPU(), size, {raw, size * 4}, {raw, size * 4}, image_size, blurMatrix, kernel_size).errorType != EasyOCL::NO_CL_ERROR)
			{
				__debugbreak();
			}
		}
		else
		{
			result = Filter::applyFilter(result, kernel, 1);
		}
	}
	return result;
}

ImageType Blur::getImageType() const
{
	return ImageType::QImage;
}

std::vector<float> Blur::createBlurMatrix() const
{
	int intensity = 1 + 2 * m_intensity;
	float sig = intensity / 6.0;

	std::vector<float> kernel;
	kernel.resize(intensity * intensity);
	int mid = std::floor(intensity / 2.0);
	int size = intensity - 1;

	auto xyToIndex = [&](int x, int y) { return y * intensity + x; };

	// iterating through 1/4-th of the image because its point-symmetrical
	for (int x = mid; x >= 0; x--)
	{
		for (int y = mid; y >= 0; y--)
		{
			kernel[xyToIndex(y, x)] = gaussian(mid - x, mid - y, sig);
			kernel[xyToIndex(size - x, y)] = gaussian(mid - x, mid - y, sig);
			kernel[xyToIndex(x, size - y)] = gaussian(mid - x, mid - y, sig);
			kernel[xyToIndex(size - x, size - y)] = gaussian(mid - x, mid - y, sig);
		}
	}

	return kernel;
}

// source: https://en.wikipedia.org/wiki/Gaussian_blur
float Blur::gaussian(float x, float y, float sig) const
{
	return 1.0f / (2.0f * 3.141592653589796 * sig * sig) * std::exp(-1.0f * (x * x + y * y) / (2.0f * sig * sig));
}

REGISTER_OPERATION(Blur);