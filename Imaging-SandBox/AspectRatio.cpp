#include "stdafx.h"
#include "AspectRatio.h"

AspectRatio::AspectRatio()
{
   REGISTER_PARAMETER(new IntegerSlider("horizontal: ", 1, 20, 1, &m_horizontal));
   REGISTER_PARAMETER(new IntegerSlider("vertical: ", 1, 20, 1, &m_vertical));
   REGISTER_PARAMETER(new ComboBox("mode: ", {"zoom in to fit", "zoom out to fit", "ignore previous aspect ratio"}, &m_mode))
}

ImageData AspectRatio::processImage(const ImageData& image) const
{
   if (!image.isQImage()) return QImage();
   auto& img = image.getQImage();

   int w = img.width();
   int h = img.height();

   auto mode =
      m_mode == "zoom in to fit" ? 1 :
      m_mode == "zoom out to fit" ? 2 :
      0;

   auto ratio = m_vertical / (float) m_horizontal;

   int nw = w;
   int nh = ratio * w;

   return img.scaled(QSize(nw, nh), static_cast<Qt::AspectRatioMode>(mode));
}

ImageType AspectRatio::getImageType() const
{
   return ImageType::QImage;
}

REGISTER_OPERATION(AspectRatio);
