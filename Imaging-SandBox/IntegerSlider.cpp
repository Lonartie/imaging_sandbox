#include "stdafx.h"
#include "IntegerSlider.h"
#include "AdvancedLabel.h"

IntegerSlider::IntegerSlider(const QString& fieldName, int min /*= 0*/, int max /*= 100*/, int step /*= 1*/, int* connected /*= nullptr*/, const QString& unit /*= ""*/)
   : name(fieldName)
   , min(min)
   , max(max)
   , step(step)
   , value(connected)
   , unit(unit)
{}

void IntegerSlider::connectValue(int* value)
{
   this->value = value;
}

QWidget* IntegerSlider::widget(QWidget* parent /*= Q_NULLPTR*/)
{
   auto container = new QWidget(parent);
   auto layout = new QHBoxLayout();
   auto label = new AdvancedLabel();
   auto slider = new QSlider(Qt::Orientation::Horizontal);
   auto val = new QLabel();

	auto w1 = val->fontMetrics().boundingRect(QString("%1%2").arg(max).arg(unit)).width();
	auto w2 = val->fontMetrics().boundingRect(QString("%1%2").arg(min).arg(unit)).width();

	label->setText(name);
	val->setFixedWidth(std::max(w1, w2));
   slider->setMinimum(min);
   slider->setMaximum(max);
   slider->setSingleStep(step);
   slider->setTickInterval(step);
   slider->setFixedWidth(100);

   if (value) val->setText(QString("%1%2").arg(*value).arg(unit));
   if (value) slider->setValue(*value);

   layout->setMargin(0);
   layout->setSpacing(0);
   layout->addWidget(label);
   layout->addWidget(slider);
   layout->addSpacing(10);
   layout->addWidget(val);

   container->setLayout(layout);

   connect(slider, &QSlider::valueChanged, [=](int valu) { if (!value) return; *value = valu; val->setText(QString("%1%2").arg(*value).arg(unit)); });
   connect(slider, &QSlider::valueChanged, this, &OperationParameter::updated);

   return container;
}
