#pragma once

#include "stdafx.h"
#include "CommonFunctions.h"

class AdvancedLabel: public QLabel
{
   Q_OBJECT

public /*ctors*/:
   
   AdvancedLabel(QWidget* parent = Q_NULLPTR);
   AdvancedLabel(const QImage& image);
   virtual ~AdvancedLabel() = default;

public /*exposed functions*/:

   void setFullResolution(bool full);
   void setDraggable(bool draggable);
   void setImage(const QImage& image);
   void setDataSuffix(const QString& suffix);
	void setContextMenu(QMenu* menu);
	void setData(const QVariant& variant);
	const QVariant& getData() const;
	QVariant& getData();

public slots:

	void grayOut(bool gray);

signals:

	void doubleClicked(const QPoint& position);
   void clicked(const QPoint& position, Qt::MouseButton button);
   void dragAccepted();
   void mouseEntered(const QPoint& position);
   void mouseLeaved();

protected /*inherited*/:

   virtual void mousePressEvent(QMouseEvent* event) override;
   virtual void mouseDoubleClickEvent(QMouseEvent* event) override;
   virtual void enterEvent(QEvent* event) override;
   virtual void leaveEvent(QEvent* event) override;
   
private /*functions*/:

	void showContextMenu();
	void setImageInUI(const QImage& image);
	QImage grayed(const QImage& image);
	QImage scaledImage() const;

private /*members*/:

	bool m_fullResolution = false;
	bool m_grayed = false;
   QImage m_image;
   QString m_dataSuffix;
   bool m_draggable = false;
	QMenu* m_contextMenu = nullptr;
	QVariant m_data;
};
