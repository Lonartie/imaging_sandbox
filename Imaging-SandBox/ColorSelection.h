#pragma once

#include "CommonFunctions.h"
#include "AdvancedLabel.h"
#include "AdvancedFrame.h"

class ColorSelection: public OperationParameter
{
public:

   ColorSelection(const QString& text, QColor* connected);

   virtual QWidget* widget(QWidget* parent = Q_NULLPTR) override;

private slots:

   void selectColor(AdvancedFrame* fram);

private:

   QString m_text;
   QColor* m_connected = nullptr;
};
