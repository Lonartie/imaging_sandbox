#include "stdafx.h"
#include "Monochrome.h"

Monochrome::Monochrome()
{
	REGISTER_PARAMETER(new IntegerSlider("percentage: ", 0, 100, 1, &value, "%"));
}

ImageData Monochrome::processImage(const ImageData& imageData) const
{
	if (!imageData.isQImage()) return QImage();
	const QImage& image = imageData.getQImage();

   QImage res(image.size(), image.format());
   OMP_LOOP_IMAGE(image)
   {
      auto pix = image.pixelColor(x, y);

      auto r = pix.red();
      auto g = pix.green();
      auto b = pix.blue();
      
      auto av = (r + g + b) / 3;

      pix.setRed(fade(r, av, value / 100.0));
      pix.setGreen(fade(g, av, value / 100.0));
      pix.setBlue(fade(b, av, value / 100.0));

      res.setPixelColor(x, y, pix);
   }
   return res;
}

ImageType Monochrome::getImageType() const
{
   return ImageType::QImage;
}

int Monochrome::fade(int a, int b, float p) const
{
   return a + ((b - a) * p);
}

REGISTER_OPERATION(Monochrome);
