#pragma once

#include "CommonFunctions.h"

class SubSampling : public Operation
{
	Q_OBJECT

	OPERATION(SubSampling);
	
public:

	SubSampling();
	virtual ImageData processImage(const ImageData& imageData) const override;

	virtual ImageType getImageType() const override;

private:

	int value = 25;
	Interpolation* m_interpolation;

};
