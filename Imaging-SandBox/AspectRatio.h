#pragma once

#include "CommonFunctions.h"

class AspectRatio: public Operation
{
   Q_OBJECT;
   OPERATION(AspectRatio);

public:
   AspectRatio();

   virtual ImageData processImage(const ImageData& image) const override;
   
   virtual ImageType getImageType() const override;

private:

   int
      m_horizontal = 16,
      m_vertical = 9;

   QString
      m_mode;

};
