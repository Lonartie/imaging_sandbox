#pragma once

#include "stdafx.h"
#include "ui_ImageView.h"

class ImageView: public QWidget
{
   Q_OBJECT

public /*ctors*/:

   ImageView(QWidget* parent = Q_NULLPTR);
   ~ImageView() = default;

public /*exposed functions*/:

   void setImage(const QImage& image);
   void setInformationVisible(bool visible);
   void setAlignment(Qt::Alignment alignment);
   void showInformationOnHover(bool show);

public slots:

	void setContextMenu(QMenu* menu) const;
	void setFullResolution(bool show) const;

private /*ui functions*/:

   void setImageInUI() const;
   void setInformationInUI() const;
   void setInformationVisibleInUI() const;

signals:

	void doubleClicked() const;

private /*members*/:

   QImage m_image;
   QGroupBox* m_informationPane;
   QSpacerItem* m_spac;
   bool m_informationVisible = true;
   bool m_showInformationOnHover = false;
   Ui::ImageView m_ui;
};
