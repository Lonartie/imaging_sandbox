#pragma once

#include <EasyOCL>

class FilterCl : public EasyOCL::KernelClass
{
	KERNEL_CLASS_H(FilterCl);
public:
	KERNEL_FUNCTION_H(applyFilter, (const unsigned char*, image_in, unsigned char*, image_out, const int*, imageSize, const float*, kernelMatrix, const int*, kernelSize));
};
