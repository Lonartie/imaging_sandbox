#pragma once

#include "stdafx.h"
#include <fftw3.h>
#include "Filter.h"

class FrequencyImage
{
public /*defs*/:

	struct PixelValue { fftw_complex red, green, blue; };

public /*ctors*/:

	FrequencyImage() = default;
	FrequencyImage(const QSize& size);
	FrequencyImage(std::size_t width, std::size_t height);
	FrequencyImage(const QImage& image);
	FrequencyImage(fftw_complex* data[3], std::size_t width, std::size_t height);
	FrequencyImage(const FrequencyImage& o);
	FrequencyImage(FrequencyImage&& o);

	~FrequencyImage();

public /*operators*/:

	FrequencyImage& operator=(const FrequencyImage& o) noexcept;
	FrequencyImage& operator=(FrequencyImage&& o) noexcept;
	FrequencyImage& operator=(const QImage& o) noexcept;

public /*exposed functions*/:
	
	PixelValue getPixel(const QPoint& position);
	void setPixel(const QPoint& position, const PixelValue& value);
	fftw_complex* getChannel(Filter::ColorChannel channel) const;
	void clear();
	void malloc();
	QImage toImage() const;
	QImage asImage() const;

	std::size_t width() const;
	std::size_t height() const;

public /*static exposed functions*/:

	static FrequencyImage fromImage(const QImage& image);
	static FrequencyImage fromFrequencies(fftw_complex* data[3], std::size_t width, std::size_t height);

private /*methods*/:

	std::size_t toIndex(std::size_t x, std::size_t y) const;
	QPoint toPoint(std::size_t index) const;

private /*members*/:

	fftw_complex* r = nullptr;
	fftw_complex* g = nullptr;
	fftw_complex* b = nullptr;

	std::size_t _width = 0;
	std::size_t _height = 0;

	bool moved = false;

};
