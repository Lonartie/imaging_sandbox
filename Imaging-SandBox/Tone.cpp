#include "stdafx.h"
#include "Tone.h"

Tone::Tone()
{
   REGISTER_PARAMETER(new IntegerSlider("tone: ", -100, 100, 1, &m_tone, "%"));
   m_tone = 0;
}

ImageData Tone::processImage(const ImageData& image) const
{
   if (!image.isQImage()) return QImage();
   auto& img = image.getQImage();
   QImage result(img.size(), img.format());

   OMP_LOOP_IMAGE(img)
   {
      auto pix = img.pixelColor(x, y);
      result.setPixelColor(x, y, tone(pix));
   }

   return result;
}

ImageType Tone::getImageType() const
{
   return ImageType::QImage;
}

QColor Tone::tone(const QColor& a) const
{
   if (m_tone < 0) return mul(a, fade(Qt::white, QColor(0, 0, 255), m_tone / -100.0f));
   if (m_tone > 0) return mul(a, fade(Qt::white, QColor(255, 128, 0), m_tone / 100.0f));
   return a;
}

QColor Tone::mul(const QColor& a, const QColor& b) const
{
   return QColor
   (
      255 * (a.red() / 255.0 * b.red() / 255.0),
      255 * (a.green() / 255.0 * b.green() / 255.0),
      255 * (a.blue() / 255.0 * b.blue() / 255.0),
      255 * (a.alpha() / 255.0 * b.alpha() / 255.0)
   );
}

QColor Tone::fade(const QColor& a, const QColor& b, float value) const
{
   return QColor
   (
      a.red() + ((b.red() - a.red()) * value),
      a.green() + ((b.green() - a.green()) * value),
      a.blue() + ((b.blue() - a.blue()) * value),
      a.alpha() + ((b.alpha() - a.alpha()) * value)
   );
}

REGISTER_OPERATION(Tone);
