#pragma once

#include "CommonFunctions.h"

class Saturation : public Operation
{
   Q_OBJECT;
   OPERATION(Saturation);

public:

   Saturation();
   
   virtual ImageData processImage(const ImageData& image) const override;
   
   virtual ImageType getImageType() const override;

private:

   QColor saturate(const QColor& a) const;

   int m_intensity = 0;

};
