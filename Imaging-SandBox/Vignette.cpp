#include "stdafx.h"
#include "Vignette.h"

Vignette::Vignette()
{
   REGISTER_PARAMETER(new IntegerSlider("radius: ", 0, 100, 1, &m_radius, "%"));
   REGISTER_PARAMETER(new IntegerSlider("fade: ", 0, 200, 1, &m_fade, "%"));

   m_radius = 80;
   m_fade = 150;
}

ImageData Vignette::processImage(const ImageData& image) const
{
   if (!image.isQImage()) return QImage();
   auto& img = image.getQImage();
   QImage result(img.size(), img.format());

   OMP_LOOP_IMAGE(img)
   {
      auto pix = img.pixelColor(x, y);
      result.setPixelColor(x, y, vignette(img.size(), {x, y}, pix));
   }

   return result;
}

ImageType Vignette::getImageType() const
{
   return ImageType::QImage;
}

QColor Vignette::vignette(const QSize& size, const QPoint& pos, const QColor& col) const
{
   auto hw = size.width() / 2;
   auto hh = size.height() / 2;
   auto relative_to_mid = pos - QPoint(hw, hh);
   auto distance_to_mid = std::sqrt(relative_to_mid.x() * relative_to_mid.x() + relative_to_mid.y() * relative_to_mid.y());
   auto radius_mapped = m_radius / 100.0 * hw;
   auto intensity_mapped = radius_mapped + (hw - radius_mapped) * m_fade / 100.0f;
   auto ratio = (distance_to_mid - radius_mapped) / (intensity_mapped - radius_mapped);
   
   ratio = std::max<double>(0, std::min<double>(1, ratio));
   
   return faded(col, Qt::black, ratio);
}

QColor Vignette::faded(const QColor& a, const QColor& b, float value) const
{
   return QColor
   (
      a.red() + ((b.red() - a.red()) * value),
      a.green() + ((b.green() - a.green()) * value),
      a.blue() + ((b.blue() - a.blue()) * value),
      a.alpha() + ((b.alpha() - a.alpha()) * value)
   );
}

REGISTER_OPERATION(Vignette);