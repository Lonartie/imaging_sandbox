#pragma once

#include "stdafx.h"
#include "OperationParameter.h"

class ComboBox: public OperationParameter
{
	Q_OBJECT;

public:
	ComboBox(const QString& label, const QStringList& options, QString* connected);
	virtual ~ComboBox() = default;

	virtual QWidget* widget(QWidget* parent = Q_NULLPTR) override;

private:
	QString m_label;
	QStringList m_options;
	QString* m_connected;
};
