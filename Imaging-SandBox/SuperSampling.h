#pragma once

#include "CommonFunctions.h"

class SuperSampling : public Operation
{
	Q_OBJECT

	OPERATION(SuperSampling);
	
public:

	SuperSampling();
	virtual ImageData processImage(const ImageData& imageData) const override;

	virtual ImageType getImageType() const override;

private:

	int value = 100;
	Interpolation* m_interpolation;

};
