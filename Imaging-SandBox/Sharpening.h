#pragma once

#include "CommonFunctions.h"

class Sharpening : public Operation
{
   Q_OBJECT;
   OPERATION(Sharpening);

public:
      
   Sharpening();

   virtual ImageData processImage(const ImageData& image) const override;

   virtual ImageType getImageType() const override;

};
