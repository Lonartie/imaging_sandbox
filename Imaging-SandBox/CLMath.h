#pragma once

#include <EasyOCL>

class CLMath : public EasyOCL::FunctionClass
{
	FUNCTION_CLASS_H(CLMath);

public:

	FUNCTION_H(index_to_x, int, (int, index, int, width));
	FUNCTION_H(index_to_y, int, (int, index, int, width));
	FUNCTION_H(xy_to_index, int, (int, x, int, y, int, width));
	FUNCTION_H(clamp_between, float, (float, min, float, max, float, val));
	FUNCTION_H(gaussian_value, float, (float, x, float, y, float, sig));
};
