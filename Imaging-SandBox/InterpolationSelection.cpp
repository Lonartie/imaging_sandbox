#include "stdafx.h"
#include "InterpolationSelection.h"
#include "InterpolationRegistry.h"

InterpolationSelection::InterpolationSelection(Interpolation** connected)
   : m_connected(connected)
{
   m_interpolations = InterpolationRegistry::instance().getInterpolations();

   for (auto& interpolation : m_interpolations)
   {
      connect(interpolation.get(), &Interpolation::updated, this, &OperationParameter::updated);
      m_names << interpolation->getName();
   }

   if (m_names.size() > 0) *m_connected = m_interpolations[0].get();
}

QWidget* InterpolationSelection::widget(QWidget* parent /*= Q_NULLPTR*/)
{

   auto container = new QWidget(parent);
   auto layout = new QVBoxLayout();
   auto scontainer = new QWidget(container);
   auto slayout = new QHBoxLayout();
   auto label = new QLabel();
   auto selection = new QComboBox();

   layout->setMargin(0);
   layout->setSpacing(0);

   label->setText("Interpolation type: ");
   selection->addItems(m_names);
   //config->setText("configure");

   if (m_names.size() > 0)
      selection->setCurrentIndex(0);

	// TODO sublayout for sub widgets under interpolation

   connect(selection, qOverload<int>(&QComboBox::currentIndexChanged), [=](int index) { if (m_connected) *m_connected = m_interpolations[index].get(); });
	connect(selection, qOverload<int>(&QComboBox::currentIndexChanged), this, &OperationParameter::updated);
   connect(selection, qOverload<int>(&QComboBox::currentIndexChanged), [=](int index)
   {
      if (m_lastWidget)
      {
         layout->removeWidget(m_lastWidget);
         m_lastWidget->deleteLater();
      }

      m_lastWidget = m_interpolations[index]->widget(scontainer);

      if (m_lastWidget)
      {
         layout->addWidget(m_lastWidget);
      }
   });

   slayout->addWidget(label);
   slayout->addWidget(selection);
   scontainer->setLayout(slayout);

   layout->addWidget(scontainer);
   container->setLayout(layout);
   return container;
}

//void InterpolationSelection::showConfig(QWidget* parent /*= Q_NULLPTR*/) const
//{
//   if (!m_connected) return;
//   if (!*m_connected) return;
//
//   Interpolation* interpolation = *m_connected;
//   auto cont = interpolation->widget();
//
//   if (!cont)
//   {
//      QMessageBox::information(parent, "no settings available", "this interpolation type has no settings");
//      return;
//   }
//
//   auto diag = new QDialog(parent);
//   auto layout = new QVBoxLayout();
//
//   diag->setWindowTitle(QString("%1 - Settings").arg(interpolation->getName()));
//   diag->setWindowFlags(diag->windowFlags() & ~Qt::WindowContextHelpButtonHint);
//
//   layout->setMargin(0);
//   layout->setSpacing(0);
//
//   if (cont) layout->addWidget(cont);
//
//   diag->setLayout(layout);
//   diag->exec();
//
//   emit updated();
//}
