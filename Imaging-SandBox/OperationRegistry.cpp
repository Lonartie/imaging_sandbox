#include "stdafx.h"
#include "OperationRegistry.h"

std::vector<std::shared_ptr<Operation>> OperationRegistry::getOperations()
{
	std::vector<std::shared_ptr<Operation>> operations;
	for (auto& operation : m_operations)
		operations.push_back(operation());
	return operations;
}

OperationRegistry& OperationRegistry::instance()
{
	static OperationRegistry _instance;
	return _instance;
}

