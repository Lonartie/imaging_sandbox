#include "stdafx.h"
#include "ImageView.h"

namespace
{
   QString formatToString(QImage::Format format);
}

ImageView::ImageView(QWidget* parent)
   : QWidget(parent)
{
   m_ui.setupUi(this);
   m_informationPane = m_ui.Information;
   m_spac = m_ui.SP_2;
   setInformationVisible(false);
   //setInformationVisible(true);
	connect(m_ui.Render, &AdvancedLabel::doubleClicked, this, &ImageView::doubleClicked);
}

void ImageView::setImage(const QImage& image)
{
   m_image = image;
   setImageInUI();
   setInformationInUI();
}

void ImageView::setInformationVisible(bool visible)
{
   m_informationVisible = visible;
   setInformationVisibleInUI();
}

void ImageView::setAlignment(Qt::Alignment alignment)
{
   layout()->setAlignment(alignment);
}

void ImageView::showInformationOnHover(bool show)
{
   m_showInformationOnHover = show;
}

void ImageView::setContextMenu(QMenu* menu) const
{
	m_ui.Render->setContextMenu(menu);
}

void ImageView::setFullResolution(bool show) const
{
	m_ui.Render->setFullResolution(show);
}

void ImageView::setImageInUI() const
{
   m_ui.Render->setPixmap(QPixmap::fromImage(m_image.scaled(QSize(150, 150), Qt::KeepAspectRatio)));
}

void ImageView::setInformationInUI() const
{
   m_ui.Width->setText(QString("%1 px").arg(m_image.width()));
   m_ui.Height->setText(QString("%1 px").arg(m_image.height()));
   m_ui.Format->setText(formatToString(m_image.format()));
}

void ImageView::setInformationVisibleInUI() const
{
   if (m_informationVisible)
   {
      m_ui.gridLayout->addWidget(m_informationPane, 0, 1, 1, 1);
      m_ui.gridLayout->addItem(m_spac, 1, 1, 1, 1);
      m_informationPane->setVisible(true);
   }
   else
   {
      m_ui.gridLayout->removeWidget(m_informationPane);
      m_ui.gridLayout->removeItem(m_spac);
      m_informationPane->setVisible(false);
   }
}

namespace
{
   QString formatToString(QImage::Format format)
   {
      switch (format)
      {
      case QImage::Format_Invalid: return "Format_Invalid";
      case QImage::Format_Mono: return "Format_Mono";
      case QImage::Format_MonoLSB: return "Format_MonoLSB";
      case QImage::Format_Indexed8: return "Format_Indexed8";
      case QImage::Format_RGB32: return "Format_RGB32";
      case QImage::Format_ARGB32: return "Format_ARGB32";
      case QImage::Format_ARGB32_Premultiplied: return "Format_ARGB32_Premultiplied";
      case QImage::Format_RGB16: return "Format_RGB16";
      case QImage::Format_ARGB8565_Premultiplied: return "Format_ARGB8565_Premultiplied";
      case QImage::Format_RGB666: return "Format_RGB666";
      case QImage::Format_ARGB6666_Premultiplied: return "Format_ARGB6666_Premultiplied";
      case QImage::Format_RGB555: return "Format_RGB555";
      case QImage::Format_ARGB8555_Premultiplied: return "Format_ARGB8555_Premultiplied";
      case QImage::Format_RGB888: return "Format_RGB888";
      case QImage::Format_RGB444: return "Format_RGB444";
      case QImage::Format_ARGB4444_Premultiplied: return "Format_ARGB4444_Premultiplied";
      case QImage::Format_RGBX8888: return "Format_RGBX8888";
      case QImage::Format_RGBA8888: return "Format_RGBA8888";
      case QImage::Format_RGBA8888_Premultiplied: return "Format_RGBA8888_Premultiplied";
      case QImage::Format_BGR30: return "Format_BGR30";
      case QImage::Format_A2BGR30_Premultiplied: return "Format_A2BGR30_Premultiplied";
      case QImage::Format_RGB30: return "Format_RGB30";
      case QImage::Format_A2RGB30_Premultiplied: return "Format_A2RGB30_Premultiplied";
      case QImage::Format_Alpha8: return "Format_Alpha8";
      case QImage::Format_Grayscale8: return "Format_Grayscale8";
      case QImage::Format_RGBX64: return "Format_RGBX64";
      case QImage::Format_RGBA64: return "Format_RGBA64";
      case QImage::Format_RGBA64_Premultiplied: return "Format_RGBA64_Premultiplied";
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 6)
      case QImage::Format_Grayscale16: return "Format_Grayscale16";
#endif
      default: return "Unknown";
      }
   }
}