#pragma once

#include "CommonFunctions.h"
#include <fftw3.h>

class ToImage: public Operation
{
   Q_OBJECT;
   OPERATION(ToImage);

public:

   virtual ImageData processImage(const ImageData& imageData) const override;
   virtual ImageType getImageType() const override;

   void imageToComplexChannels(const QImage& image, fftw_complex* out, Filter::ColorChannel channel) const;
   QImage imageFromComplexChannels(const std::vector<fftw_complex*>& channels, int width, int height) const;


};
