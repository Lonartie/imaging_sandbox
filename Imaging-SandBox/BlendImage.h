#pragma once

#include "CommonFunctions.h"

class BlendImage : public Operation
{
   Q_OBJECT;
   OPERATION(BlendImage);

public:

   BlendImage();
   
   virtual ImageData processImage(const ImageData& imageData) const override;

   virtual ImageType getImageType() const override;

private:

	QColor faded(const QColor& a, const QColor& b, float value) const;
	QColor add(const QColor& a, const QColor& b) const;
	QColor sub(const QColor& a, const QColor& b) const;
	QColor mul(const QColor& a, const QColor& b) const;

   QImage m_image;
	QStringList m_methods;
	QString m_method;
   int m_value = 50;
};
