#pragma once

#include "stdafx.h"
#include "ui_OperationList.h"
#include "Operation.h"

class OperationList: public QWidget
{
   Q_OBJECT

public /*ctors*/:

   OperationList(QWidget* parent = Q_NULLPTR);
   ~OperationList();

public /*exposed functions*/:

   void addOperation(Operation* operation);
	void imageSelection(bool on);

signals:

	void operationDoubleClicked(Operation* operation) const;

protected /*inherited functions*/:

   virtual void dragEnterEvent(QDragEnterEvent* event) override;
   virtual void dropEvent(QDropEvent* event) override;

private slots:

   void addOperationInUI(Operation* operation) const;
   void search(const QString& text);
   void updateVisibilities();

private /*members*/:

   QList<Operation*> m_operations;
   QString m_searchString;
   Ui::OperationList m_ui;
};
