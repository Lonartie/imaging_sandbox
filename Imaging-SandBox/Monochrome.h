#pragma once

#include "CommonFunctions.h"

class Monochrome : public Operation
{
   Q_OBJECT;

   OPERATION(Monochrome);

public:

   Monochrome();

   virtual ImageData processImage(const ImageData& imageData) const override;
   
   virtual ImageType getImageType() const override;

private:

   int fade(int a, int b, float p) const;
   int value = 100;
};
