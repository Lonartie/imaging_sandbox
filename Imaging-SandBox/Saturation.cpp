#include "stdafx.h"
#include "Saturation.h"

Saturation::Saturation()
{
   REGISTER_PARAMETER(new IntegerSlider("intensity: ", -200, 200, 1, &m_intensity, "%"));
   m_intensity = 0;
}

ImageData Saturation::processImage(const ImageData& image) const
{
   if (!image.isQImage()) return QImage();
   auto& img = image.getQImage();
   QImage result(img.size(), img.format());

   OMP_LOOP_IMAGE(img)
   {
      auto pix = img.pixelColor(x, y);
      result.setPixelColor(x, y, saturate(pix));
   }

   return result;
}

ImageType Saturation::getImageType() const
{
   return ImageType::QImage;
}

QColor Saturation::saturate(const QColor& a) const
{
   int r = a.red();
   int g = a.green();
   int b = a.blue();

   float vr = r / 255.0f - 1.0f;
   float vg = g / 255.0f - 1.0f;
   float vb = b / 255.0f - 1.0f;

   vr *= m_intensity / 100.0f + 1.0f;
   vg *= m_intensity / 100.0f + 1.0f;
   vb *= m_intensity / 100.0f + 1.0f;

   int tr = (vr + 1.0f) * 255.0f;
   int tg = (vg + 1.0f) * 255.0f;
   int tb = (vb + 1.0f) * 255.0f;

   return QColor
   (
      std::max(0, std::min(255, tr)),
      std::max(0, std::min(255, tg)),
      std::max(0, std::min(255, tb))
   );
}
REGISTER_OPERATION(Saturation);