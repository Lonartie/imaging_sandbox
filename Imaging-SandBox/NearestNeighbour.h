#pragma once

#include "CommonFunctions.h"

class NearestNeighbour : public Interpolation
{
	Q_OBJECT;
	INTERPOLATION(NearestNeighbour);

public:

	virtual QColor interpolate(const QImage& image, const QPointF& required) const override;

private:

	QPoint clamp(const QSize& size, const QPoint& point) const;
};
