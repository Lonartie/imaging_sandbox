//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"

namespace EasyOCL
{

	QString EASYOCL_EXPORT __format(const QString& fn, bool is_func = true);

	template<typename T>
	bool inline __add_class_function(const QString& a, const QString& b)
	{
		T::__class_functions.emplace(a, __format(b));
		return true;
	}
}