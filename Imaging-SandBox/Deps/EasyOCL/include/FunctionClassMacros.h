//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "CommonMacros.h"
#include "FunctionalMacros.h"

#define FUNCTION_CLASS_H(CLASS)																\
public:																								\
	static std::map<QString /*name*/, QString /*source*/> __class_functions;	\
	static std::map<std::size_t /*hash*/, QString /*name*/> __hash_map;			\
	static std::vector<QString> __includes;												\
	static std::vector<QString> __headers;													\
	static QString getFunctionSource(const QString& name);							\
	template<typename RET, typename ...ARGS>												\
	static QString getFunctionSource(RET(*fn_pointer)(ARGS...))						\
	{																									\
		return __class_functions[__hash_map[FUNCTION_HASH(fn_pointer)]];			\
	}																									\
	static QString getAll();																	\
private:

#define FUNCTION_CLASS_CPP_BEGIN(CLASS)													\
__pragma(warning(push))																			\
__pragma(warning(disable: 4002))																\
__pragma(warning(disable: 4003))																\
std::map<QString /*name*/, QString /*source*/> CLASS::__class_functions;		\
std::map<std::size_t /*hash*/, QString /*name*/> CLASS::__hash_map;				\
std::vector<QString> CLASS::__includes;													\
std::vector<QString> CLASS::__headers;														\
QString CLASS::getFunctionSource(const QString& name)									\
{																										\
	return __class_functions[name];															\
}																										\
QString CLASS::getAll()																			\
{																										\
	QString result = QString("/* included from %1 [Declaration]:*/\n")			\
	.arg(STRING(CLASS));																			\
	for (const auto& header : __headers)													\
		result += header + QString("\n");													\
	result += QString("/* included from %1 [Definition]:*/\n").arg(STRING(CLASS));\
	for (const auto& kvp : __class_functions)												\
		result += kvp.second + QString("\n");												\
	return result;																					\
}

#define FUNCTION_CLASS_CPP_END()																\
__pragma(warning(pop))

#define FUNCTION_H(NAME, RET_TYPE, ARGS)													\
static RET_TYPE NAME ( ARGUMENT_HEADER ARGS );											\

#define FUNCTION_CPP(CLASS, NAME, RET_TYPE, ARGS, FUNC)								\
RET_TYPE CLASS::NAME ( ARGUMENT_HEADER ARGS ) FUNC ;									\
static bool CAT(CAT(CLASS, NAME), _REGISTERED) = [&]()								\
{																										\
	CLASS::__class_functions.emplace(STRING(CLASS::NAME),								\
	EasyOCL::__format(STRING(RET_TYPE NAME(ARGUMENT_HEADER ARGS) FUNC, true)));\
	auto fn_pointer = &CLASS::NAME;															\
	CLASS::__hash_map.emplace(FUNCTION_HASH(fn_pointer), STRING(CLASS::NAME)); \
	CLASS::__headers.push_back(EasyOCL::__format(										\
	STRING(RET_TYPE NAME(ARGUMENT_HEADER ARGS);), true));								\
	return true;																					\
}();																									
