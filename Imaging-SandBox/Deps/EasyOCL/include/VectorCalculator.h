//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "EasyOCL"

/************************************************************/
/* Simple demonstration of kernel class usage, nothing more */
/************************************************************/

namespace EasyOCL
{
	namespace Demo
	{
		/************************************************************************/
		/*									    DECLARATION										*/
		/************************************************************************/

		class VectorCalculator: public EasyOCL::KernelClass
		{
			KERNEL_CLASS_H(VectorCalculator);

		public:

			// default constructor
			VectorCalculator() = default;

			// constructor automatically compiling the given function for the given device
			template<typename ...ARGS>
			VectorCalculator(EasyOCL::ExecutionResult(*fn)(ARGS...), EasyOCL::OCLDevice device)
			{
				compile(fn, device);
			}

			// compiles all functions for all found devices
			void compileAll() const;

			KERNEL_FUNCTION_H(add, (const float*, a, const float*, b, float*, result));
			KERNEL_FUNCTION_H(sub, (const float*, a, const float*, b, float*, result));
			KERNEL_FUNCTION_H(mul_single, (const float*, a, const float*, b, float*, result));
			KERNEL_FUNCTION_H(div_single, (const float*, a, const float*, b, float*, result));
			KERNEL_FUNCTION_H(mag, (const float*, a, int*, size, float*, tmp, float*, result));
		};



		/************************************************************************/
		/*										    DEFINITION										*/
		/************************************************************************/




		KERNEL_CLASS_CPP_BEGIN(VectorCalculator);

		void VectorCalculator::compileAll() const
		{
			compile("add");
			compile("sub");
			compile("mul_single");
			compile("div_single");
			compile("mag");
		}

		KERNEL_FUNCTION_CPP(VectorCalculator, add, (const float*, a, const float*, b, float*, result),
		{
			const int id = EasyOCL::get_global_id(0);
			result[id] = a[id] + b[id];
		});

		KERNEL_FUNCTION_CPP(VectorCalculator, sub, (const float*, a, const float*, b, float*, result),
		{
			const int id = EasyOCL::get_global_id(0);
			result[id] = a[id] - b[id];
		});

		KERNEL_FUNCTION_CPP(VectorCalculator, mul_single, (const float*, a, const float*, b, float*, result),
		{
			const int id = EasyOCL::get_global_id(0);
			result[id] = a[id] * b[0];
		});

		KERNEL_FUNCTION_CPP(VectorCalculator, div_single, (const float*, a, const float*, b, float*, result),
		{
			const int id = EasyOCL::get_global_id(0);
			result[id] = a[id] / b[0];
		});

		KERNEL_FUNCTION_CPP(VectorCalculator, mag, (const float*, a, int*, size, float*, tmp, float*, result),
		{
			const int id = EasyOCL::get_global_id(0);

			//TODO: do something here

		});

		KERNEL_CLASS_CPP_END();
	}
}