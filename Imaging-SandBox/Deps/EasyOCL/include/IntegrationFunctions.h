//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"

namespace EasyOCL
{
	/// @brief					get the current index in \a dimension
	/// @note					OpenCL only function, do not use in C++!
	/// @param dimension		the dimension to get the index from
	/// @returns				the current index
	int EASYOCL_EXPORT get_global_id(int dimension);

   static const int CLK_LOCAL_MEM_FENCE = 1;
   static const int CLK_GLOBAL_MEM_FENCE = 2;

	void barrier(int type);

	template<typename ...ARGS>
	void printf(const char* message, ARGS...args) { throw std::runtime_error("do not use in cpp"); };

}