//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"
#include "ErrorType.h"
#include "ExecutionParameters.h"
#include "ErrorClass.h"

namespace EasyOCL
{
   namespace __
   {
      template<AccessType accessType> struct Reader {};

      template<> struct Reader<AccessType::READ>
      { template<typename T> static const T* read(ExecutionParameter& par) { return static_cast<const T*>(par.r_data); } };
      template<> struct Reader<AccessType::WRITE>
      { template<typename T> static T* read(ExecutionParameter& par) { return static_cast<T*>(par.w_data); } };
      template<> struct Reader<AccessType::READ_WRITE>
      { template<typename T> static T* read(ExecutionParameter& par) { return static_cast<T*>(par.rw_data); } };
   }

   // you have to handle memory for yourself!
   struct ExecutionResult : public ErrorClass
   {
      ExecutionParameters data;

      template<typename T, AccessType accessType> 
      auto getData(std::size_t index) -> typename std::conditional<accessType == READ, const T*, T*>::type 
      {
         return __::Reader<accessType>::read<T>(data[index]);
      }
   };
}