//--------------------------------------------------------------------------------------------------
//
// Copyright (C) 2020 Leon Gierschner.  All Rights Reserved.
//
//--------------------------------------------------------------------------------------------------

#pragma once

#include "stdafx.h"

namespace EasyOCL
{
	enum AccessType
	{
		READ,
		WRITE,
		READ_WRITE
	};

	struct ExecutionParameter
	{
		ExecutionParameter() = default;

		template<typename T>
		ExecutionParameter(AccessType type, const std::vector<T>& list);
		template<typename T>
		ExecutionParameter(AccessType type, std::vector<T>& list);
		template<typename T>
		ExecutionParameter(AccessType type, const T* list, std::size_t size);
		template<typename T>
		ExecutionParameter(AccessType type, T* list, std::size_t size);
		template<typename T>
		ExecutionParameter(T* list, std::size_t size);
		template<typename T>
		ExecutionParameter(const T* list, std::size_t size);

		const void* r_data = nullptr;
		void* w_data = nullptr;
		void* rw_data = nullptr;

		std::size_t dataSize = 0;
		std::size_t byteSize = 0;
		AccessType accessType = READ;
	};

	template<typename T>
	EasyOCL::ExecutionParameter::ExecutionParameter(const T* list, std::size_t size)
	{
		accessType = READ;
		r_data = list;

		byteSize = sizeof(T);
		dataSize = size;
	}

	template<typename T>
	EasyOCL::ExecutionParameter::ExecutionParameter(T* list, std::size_t size)
	{
		accessType = READ_WRITE;
		rw_data = list;

		byteSize = sizeof(T);
		dataSize = size;
	}

	template<typename T>
	EasyOCL::ExecutionParameter::ExecutionParameter(AccessType type, const T* list, std::size_t size)
	{
		accessType = type;

		if (type == READ) r_data = list;

		byteSize = sizeof(T);
		dataSize = size;
	}

	template<typename T>
	EasyOCL::ExecutionParameter::ExecutionParameter(AccessType type, T* list, std::size_t size)
	{
		accessType = type;

		if (type == READ) r_data = list;
		else if (type == WRITE) w_data = list;
		else if (type == READ_WRITE) rw_data = list;

		byteSize = sizeof(T);
		dataSize = size;

	}

	template<typename T>
	EasyOCL::ExecutionParameter::ExecutionParameter(AccessType type, const std::vector<T>& list)
	{
		accessType = type;

		if (type == READ) r_data = list.data();

		byteSize = sizeof(T);
		dataSize = list.size();
	}

	template<typename T>
	EasyOCL::ExecutionParameter::ExecutionParameter(AccessType type, std::vector<T>& list)
	{
		accessType = type;

		if (type == READ) r_data = list.data();
		else if (type == WRITE) w_data = list.data();
		else if (type == READ_WRITE) rw_data = list.data();

		byteSize = sizeof(T);
		dataSize = list.size();
	}

	using ExecutionParameters = std::vector<ExecutionParameter>;

	template<typename T> struct Parameter 
	{
		Parameter(T* list, std::size_t size): param(list, size) {};
		Parameter(std::vector<T>& list): param(list.data(), list.size()) {};
		Parameter(const std::nullptr_t): param() {};

		ExecutionParameter param;
	};

	template<typename T> struct Parameter<const T>
	{
		Parameter(const T* list, std::size_t size): param(list, size) {};
		Parameter(const std::vector<T>& list): param(list.data(), list.size()) {};
		Parameter(const std::nullptr_t): param() {};

		ExecutionParameter param;
	};
}