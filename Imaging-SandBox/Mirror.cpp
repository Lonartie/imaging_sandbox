#include "stdafx.h"
#include "Mirror.h"

Mirror::Mirror()
{
   REGISTER_PARAMETER(new ComboBox("axis: ", {"vertical", "horizontal", "diagonal"}, &m_axis));
}

ImageData Mirror::processImage(const ImageData& image) const
{
   if (!image.isQImage()) return QImage();
   auto& img = image.getQImage();
   QImage result(img.size(), img.format());

   OMP_LOOP_IMAGE(img)
   {
      auto pix = img.pixelColor(x, y);

      int tx = m_axis == "vertical" || m_axis == "diagonal" ? img.width() - x - 1 : x;
      int ty = m_axis == "horizontal" || m_axis == "diagonal" ? img.height() - y - 1 : y;

      result.setPixelColor(tx, ty, pix);
   }

   return result;
}

ImageType Mirror::getImageType() const
{
   return ImageType::QImage;
}

REGISTER_OPERATION(Mirror);
