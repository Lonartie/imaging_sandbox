#include "stdafx.h"
#include "ComboBox.h"

ComboBox::ComboBox(const QString& label, const QStringList& options, QString* connected)
	: m_label(label)
	, m_options(options)
	, m_connected(connected)
{
	if (m_connected && options.size() > 0)
	{
		*m_connected = options.first();
	}
}

QWidget* ComboBox::widget(QWidget* parent /*= Q_NULLPTR*/)
{
	auto container = new QWidget(parent);
	auto layout = new QHBoxLayout();
	auto label = new QLabel();
	auto box = new QComboBox();

	layout->setSpacing(0);
	layout->setMargin(0);

	box->setFixedWidth(150);
	label->setText(m_label);
	box->addItems(m_options);
	if(m_connected) box->setCurrentText(*m_connected);

	connect(box, &QComboBox::currentTextChanged, [&](auto& text)
	{ 
		if (!m_connected) return;
		*m_connected = text;
		emit updated();
	});

	layout->addWidget(label, 0, Qt::AlignLeft);
	layout->addWidget(box, 0, Qt::AlignRight);

	container->setLayout(layout);
	return container;
}
