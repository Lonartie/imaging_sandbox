#pragma once

#include "stdafx.h"
#include "FrequencyImage.h"

class ImageData
{
public /*ctors*/:

	ImageData() = default;
	ImageData(const ImageData&) = default;
	ImageData(ImageData&&) = default;

	ImageData(const QImage& image);
	ImageData(const FrequencyImage& image);

	~ImageData() = default;

public /*operators*/:

	operator QImage&();
	operator const QImage&() const;
	operator FrequencyImage&();
	operator const FrequencyImage&() const;

public /*exposed functions*/:

	bool isQImage() const;
	bool isFreqImage() const;

	const QImage& getQImage() const;
	QImage& getQImage();

	const FrequencyImage& getFreqImage() const;
	FrequencyImage& getFreqImage();

	const QImage& asQImage() const;
	QImage& asQImage();

	std::size_t width() const;
	std::size_t height() const;

private:

	bool is_qimage = false;
	bool is_fimage = false;

	QImage qimage;
	FrequencyImage fimage;
	QImage asqimage;
};
