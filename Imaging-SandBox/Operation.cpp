#include "stdafx.h"
#include "Operation.h"

Operation::Operation()
{
}

Operation::~Operation()
{
	for (auto& param : parameters)
		delete param;
}

QWidget* Operation::widget(QWidget* parent /*= Q_NULLPTR*/) const
{
	if (parameters.size() == 0) return nullptr;

	auto container = new QWidget(parent);
	auto layout = new QVBoxLayout();
	
	layout->setMargin(0);
	layout->setSpacing(0);

	for (auto& param : parameters)
		layout->addWidget(param->widget(parent));

	container->setLayout(layout);
	return container;
}

void Operation::execute(const ImageData& image)
{
   emit imageReady(this->processImage(image));
}

void Operation::emit_updated()
{
	emit updated();
}
