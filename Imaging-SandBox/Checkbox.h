#pragma once

#include "stdafx.h"
#include "OperationParameter.h"

class Checkbox : public OperationParameter
{
	Q_OBJECT;

public:
	Checkbox(const QString& label, bool* connected);

	virtual QWidget* widget(QWidget* parent = Q_NULLPTR) override;

private:
	QString m_text;
	bool* m_connected = nullptr;

};
