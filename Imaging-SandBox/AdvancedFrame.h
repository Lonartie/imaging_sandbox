#pragma once

#include "stdafx.h"

class AdvancedFrame: public QFrame
{
   Q_OBJECT;

public /*ctors*/:

   AdvancedFrame(QWidget* parent = Q_NULLPTR);

public /*exposed functions*/:

   void setDraggable(bool draggable);
   void setContextMenu(QMenu* menu);
   void setDataSuffix(const QString& suffix);
   void setAdditionalData(const QVariant& data);

signals:

	void clicked(const QPoint& position);
	void doubleClicked(const QPoint& position);
   void dragAccepted();
	void mouseEntered();
	void mouseLeaved();

protected /*inherited*/:

   void mousePressEvent(QMouseEvent* event) override;
	void enterEvent(QEvent *event) override;
	void leaveEvent(QEvent *event) override;
	void mouseDoubleClickEvent(QMouseEvent* event) override;

private /*members*/:

   QString m_dataSuffix;
   QVariant m_additionalData;
   bool m_draggable = false;
   QMenu* m_contextMenu = nullptr;
};
