#pragma once

#include "stdafx.h"
#include "ui_ImageList.h"
#include "AdvancedLabel.h"

class ImageList: public QWidget
{
   Q_OBJECT

public /*ctors*/:

   ImageList(QWidget* parent = Q_NULLPTR);
   ~ImageList() = default;

public /*exposed functions*/:

   std::size_t addImage(const QImage& image);
   QImage getImage(std::size_t id) const;
   void removeImage(std::size_t id);

public slots:

	void imageSelection(bool on);

signals:

	void imageDoubleClicked(const QImage& image) const;
	void imageSelected(const QImage& image) const;

private /*ui functions*/:

	void addImageToUI(const QImage& image, std::size_t id);
	void removeUIElement(QWidget* render) const;
	QMenu* generateMenu(std::size_t id);

private /*members*/:

	bool m_imageSelectionActive = false;
	QList<AdvancedLabel*> m_labels;
   QMap<std::size_t, QPair<QImage, QList<QWidget*>>> m_imageList;
   QImage m_empty;
   Ui::ImageList m_ui;

   static std::size_t idCounter;
};
