#include "stdafx.h"
#include "Versioning.h"

const std::size_t Versioning::major = 0;
const std::size_t Versioning::minor = 3;
const std::size_t Versioning::build = 2002172004;