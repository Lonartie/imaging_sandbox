#include "stdafx.h"
#include "NearestNeighbour.h"

QColor NearestNeighbour::interpolate(const QImage& image, const QPointF& required) const
{
   QPoint nearestPoint(std::round(required.x()), std::round(required.y()));
   return image.pixelColor(clamp(image.size(), nearestPoint));
}

QPoint NearestNeighbour::clamp(const QSize& size, const QPoint& point) const
{
   return
   {
      point.x() < 0 ? 0 : point.x() >= size.width() ? size.width() - 1 : point.x(),
      point.y() < 0 ? 0 : point.y() >= size.height() ? size.height() - 1 : point.y(),
   };
}

REGISTER_INTERPOLATION(NearestNeighbour);