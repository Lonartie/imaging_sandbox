#include "stdafx.h"
#include "ImageList.h"
#include "AdvancedLabel.h"

extern bool TouchDevice;
std::size_t ImageList::idCounter = 1;

ImageList::ImageList(QWidget* parent)
	: QWidget(parent)
{
	m_ui.setupUi(this);
	if (TouchDevice) QScroller::grabGesture(m_ui.ScrollArea, QScroller::LeftMouseButtonGesture);
}

std::size_t ImageList::addImage(const QImage& image)
{
	auto id = idCounter;
	m_imageList.insert(id, QPair<QImage, QList<QWidget*>> {image, {}});
	addImageToUI(image, id);
	idCounter++;
	return id;
}

QImage ImageList::getImage(std::size_t id) const
{
	return m_imageList.value(id, QPair<QImage, QList<QWidget*>> {m_empty, {}}).first;
}

void ImageList::removeImage(std::size_t id)
{
	auto labels = m_imageList.value(id, QPair<QImage, QList<QWidget*>>{m_empty, {}}).second;
	m_imageList.remove(id);

	for (auto& label : labels)
	{
		auto lbl = qobject_cast<AdvancedLabel*>(label);

		if (lbl && m_labels.contains(lbl))
			m_labels.removeAll(lbl);

		removeUIElement(label);
	}
}

void ImageList::imageSelection(bool on)
{
	m_imageSelectionActive = on;
	static auto sheet = "";
	//static auto sheet = "QWidget { background-color: lightgray; }";

	if (on)
	{
		setStyleSheet(sheet);
		m_ui.ScrollArea->setStyleSheet(sheet);
		m_ui.ImageContent->setStyleSheet(sheet);
	}
	else
	{
		setStyleSheet("");
		m_ui.ScrollArea->setStyleSheet("");
		m_ui.ImageContent->setStyleSheet("");
	}

	for (auto& label : m_labels)
	{
		label->grayOut(on);
	}
}

void ImageList::addImageToUI(const QImage& image, std::size_t id)
{
	auto label = new AdvancedLabel();
	auto line = new QFrame();

	m_labels << label;
	m_imageList[id].second.push_back(label);
	m_imageList[id].second.push_back(line);

	label->setDraggable(true);
	label->setAlignment(Qt::AlignCenter);
	label->setImage(image);
	label->setData(id);
	label->setContextMenu(generateMenu(id));
	label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	line->setFrameShape(QFrame::HLine);

	m_ui.ImageContent->layout()->addWidget(label);
	//m_ui.ImageContent->layout()->addWidget(line);
	connect(label, &AdvancedLabel::doubleClicked, [=]() { emit imageDoubleClicked(image); });
	connect(label, &AdvancedLabel::clicked, [=](auto& pos, auto btn) { if (m_imageSelectionActive && btn == Qt::LeftButton) emit imageSelected(image); });
	connect(label, &AdvancedLabel::mouseEntered, this, [=]() { if (m_imageSelectionActive) label->grayOut(false); });
	connect(label, &AdvancedLabel::mouseLeaved, this, [=]() { if (m_imageSelectionActive) label->grayOut(true); });
}

void ImageList::removeUIElement(QWidget* render) const
{
	m_ui.ImageContent->layout()->removeWidget(render);
	render->deleteLater();
}

QMenu* ImageList::generateMenu(std::size_t id)
{
	auto label = qobject_cast<AdvancedLabel*>(m_imageList[id].second.first());
	auto menu = new QMenu(this);

	auto action = new QAction("Remove");
	connect(action, &QAction::triggered, [=]() { removeImage(label->getData().toInt()); });

	auto action2 = new QAction("Remove all");
	connect(action2, &QAction::triggered, [=]() { for (auto& key : this->m_imageList.keys()) removeImage(key); });

	auto action3 = new QAction("Insert to pipeline");
	connect(action3, &QAction::triggered, [=]() { emit imageDoubleClicked(m_imageList[id].first); });

	menu->addAction(action);
	menu->addAction(action2);
	menu->addAction(action3);

	return menu;
}
