#pragma once

#include <QtCore/QString>

struct Versioning
{
	static const std::size_t major;
	static const std::size_t minor;
	static const std::size_t build;

	static QString getVersionString()
	{
		return QString("%1.%2.%3")
			.arg(major)
			.arg(minor)
			.arg(build, 5, 10, QChar('0'));
	}
};