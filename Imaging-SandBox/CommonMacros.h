#pragma once

#include "stdafx.h"
#include <EasyOCL>

#define UNIQUE(NAME) CAT(CAT(NAME, _), __LINE__)

#define OPERATION(NAME) \
public: QString getName() const override { return STRING(NAME); } \
public: Operation* copy() const override { return new NAME; } \
private: static const bool operationAdded;

#define INTERPOLATION(NAME) \
public: QString getName() const override { return STRING(NAME); } \
public: Interpolation* copy() const override { return new NAME; } \
private: static const bool interpolationAdded;

#define REGISTER_OPERATION(NAME) \
const bool NAME::operationAdded = OperationRegistry::instance().registerOperation<NAME>();

#define REGISTER_INTERPOLATION(NAME) \
const bool NAME::interpolationAdded = InterpolationRegistry::instance().registerInterpolation<NAME>();

#define REGISTER_PARAMETER(PARAM) \
auto UNIQUE(par) = PARAM; \
parameters.push_back(UNIQUE(par)); \
connect(UNIQUE(par), SIGNAL(updated()), this, SIGNAL(updated()));

#define LOOP_IMAGE(image) for(int x = 0; x < image.width(); x++) for (int y = 0; y < image.height(); y++)

#define OMP_LOOP_IMAGE(image) __pragma(omp parallel for) LOOP_IMAGE(image)

