#include "stdafx.h"
#include "Sharpening.h"
#include "FilterCl.h"

Sharpening::Sharpening()
{
   if (!FilterCl::compile(&FilterCl::applyFilter, EasyOCL::OCLDevice::GPU()))
   {
      auto& prgm = FilterCl::getProgram(&FilterCl::applyFilter, EasyOCL::OCLDevice::GPU());
      __debugbreak();
   }
}

ImageData Sharpening::processImage(const ImageData& image) const
{
   if (!image.isQImage()) return QImage();
   auto& img = image.getQImage();
   QImage result(img.size(), img.format());

   std::size_t size = img.width() * img.height();
   std::vector<int> _size = {img.width(), img.height()};
   std::vector<int> kernelSize = {3, 3};
   std::vector<float> kernel = {0, -1, 0, -1, 5, -1, 0, -1, 0};

   auto cl_res = FilterCl::applyFilter(EasyOCL::OCLDevice::GPU(), size, {img.bits(), size * 4}, {result.bits(), size * 4}, _size, kernel, kernelSize);
   if(cl_res.errorType != EasyOCL::NO_CL_ERROR)
   {
      __debugbreak();
   }

   return result;
}

ImageType Sharpening::getImageType() const
{
   return ImageType::QImage;
}

REGISTER_OPERATION(Sharpening);
