#pragma once

#include "CommonFunctions.h"

class Rotate : public Operation
{
   Q_OBJECT;
   OPERATION(Rotate);

public:

   Rotate();

   virtual ImageData processImage(const ImageData& image) const override;
   
   virtual ImageType getImageType() const override;

private:

   QPointF rotate(const QPointF& p) const;

   int m_degree = 0;
   Interpolation* m_interpolation;
};
