#pragma once
#include "stdafx.h"

class OperationParameter: public QObject
{
	Q_OBJECT;

public:	
	OperationParameter() = default;
	virtual ~OperationParameter() = default;

	virtual QWidget* widget(QWidget* parent = Q_NULLPTR) = 0;

signals:
	void updated() const;
};