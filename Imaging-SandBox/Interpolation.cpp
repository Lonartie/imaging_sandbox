#include "stdafx.h"
#include "Interpolation.h"

Interpolation::Interpolation()
{}

QWidget* Interpolation::widget(QWidget* parent /*= Q_NULLPTR*/) const
{
	if (parameters.size() == 0) return nullptr;

	auto container = new QWidget(parent);
	auto layout = new QVBoxLayout();

	layout->setMargin(0);
	layout->setSpacing(0);

	for (auto& param : parameters)
		layout->addWidget(param->widget(parent));

	container->setLayout(layout);
	return container;
}
