#include "stdafx.h"
#include "BilinearInterpolation.h"

QColor BilinearInterpolation::interpolate(const QImage& image, const QPointF& required) const
{
	double s_x = required.x() - std::floor(required.x());
	double s_y = required.y() - std::floor(required.y());

	QColor _1 = image.pixelColor(clamp(image.size(), QPoint(std::ceil(required.x()), std::ceil(required.y()))));
	QColor _2 = image.pixelColor(clamp(image.size(), QPoint(std::ceil(required.x()), std::floor(required.y()))));
	QColor _3 = image.pixelColor(clamp(image.size(), QPoint(std::floor(required.x()), std::ceil(required.y()))));
	QColor _4 = image.pixelColor(clamp(image.size(), QPoint(std::floor(required.x()), std::floor(required.y()))));

	QColor av1 = faded(_2, _1, s_y);
	QColor av2 = faded(_4, _3, s_y);

	return faded(av2, av1, s_x);
}

QPoint BilinearInterpolation::clamp(const QSize& size, const QPoint& point) const
{
	return
	{
      point.x() < 0 ? 0 : point.x() >= size.width() ? size.width() - 1 : point.x(),
      point.y() < 0 ? 0 : point.y() >= size.height() ? size.height() - 1 : point.y(),
	};
}

QColor BilinearInterpolation::faded(const QColor& a, const QColor& b, float value) const
{
	return QColor 
	(
		a.red() + ((b.red() - a.red()) * value),
		a.green() + ((b.green() - a.green()) * value),
		a.blue() + ((b.blue() - a.blue()) * value),
		a.alpha() + ((b.alpha() - a.alpha()) * value)
	);
}

REGISTER_INTERPOLATION(BilinearInterpolation);